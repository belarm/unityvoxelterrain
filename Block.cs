using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public static class BType
{
	public const int AIR = 0;
	public const int NOTAIR = 1;
	//	public const uint SW = 2;
	//	public const uint SE = 3;
}


public class Block
{
	[DebuggerDisplay ("X:{X} Y:{Y} Z:{Z}")]
	public enum Direction
	{
		north,
		east,
		south,
		west,
		up,
		down}

	;

	public static Vector3[] Corners;
	const float TileSize = 0.25f;

	public int X, Y, Z;
	public int BlockStatus = -1;
	public int Type = BType.NOTAIR;
	public bool BeenChecked = false;
	public Vector3 Position;
	//	private List<Block> neighbors;


	static Block ()
	{
		Corners = new Vector3[8];
		Corners [0] = new Vector3 (-1, 1, -1) / 2;
		Corners [1] = new Vector3 (1, 1, -1) / 2;
		Corners [2] = new Vector3 (1, -1, -1) / 2;
		Corners [3] = new Vector3 (-1, -1, -1) / 2;
		Corners [4] = new Vector3 (-1, 1, 1) / 2;
		Corners [5] = new Vector3 (1, 1, 1) / 2;
		Corners [6] = new Vector3 (1, -1, 1) / 2;
		Corners [7] = new Vector3 (-1, -1, 1) / 2;
	}

	public Block ()
	{
	}

	public Block (int px, int py, int pz)
	{
		X = px;
		Y = py;
		Z = pz;
//		bStatus = -1;
		Position = new Vector3 (X, Y, Z);
	}

	public override bool Equals (System.Object obj)
	{
		return obj != null && Equals (obj as Block);
//		if (obj == null)
//			return false;
//		Block b = obj as Block;
//		return (System.Object)b != null && ((b.X == X) && (b.Y == Y) && (b.Z == Z));
	}

	public bool Equals (Block b)
	{
		if ((object)b == null)
			return false;
		return ((b.X == X) && (b.Y == Y) && (b.Z == Z));
	}

	public override int GetHashCode ()
	{
		unchecked {
			return (X << 20 & Y << 10 & Z).GetHashCode ();
		}
	}

	public void SetPosition (int px, int py, int pz)
	{
		X = px;
		Y = py;
		Z = pz;
		Position = new Vector3 (X, Y, Z);
	}

	public void SetStatus (int stat)
	{
		BlockStatus = stat;
		GetParent ().updated = true;
	}

	public int GetAddr ()
	{
		return Morton.Encode (X, Z);
	}

	public QTNode GetQuad ()
	{
		return World.gWorld.FindQuad (GetAddr (), Y);
	}

	public Chunk GetParent ()
	{
		return World.chunks [X / World.chunkSize, Y, Z / World.chunkSize];
	}

	public int[] GetPos ()
	{
		int[] ret = new int[]{ X, Y, Z };
		return ret;
	}

	public virtual bool CheckBlock (Block end)
	{
		return ((end.GetParent () == GetParent ()) && !IsBlocking ());
	}

	public float DistanceToBlock (Block dest)
	{
		return Vector3.Distance (Position, dest.Position);
	}

	public float DistanceToPosition (Vector3 dest)
	{
		return Vector3.Distance (Position, dest);
	}

	public virtual bool IsAir ()
	{
		return Type == BType.AIR;
	}

	public virtual bool IsBlocking ()
	{
		return true;
	}

	public virtual bool IsSolid (Direction dir)
	{
		switch (dir) {
		case Direction.north:
			return true;
		case Direction.east:
			return true;
		case Direction.south:
			return true;
		case Direction.west:
			return true;
		case Direction.up:
			return true;
		case Direction.down:
			return true;
		}
		return false;
	}

	public virtual List<Block> NeighborsOrtho (Block end)
	{
		Block n;
		List<Block> res = new List<Block> ();
		if (((n = World.getBlock (X + 1, Y, Z)) != null) && n.CheckBlock (end))
			res.Add (n);
		if (((n = World.getBlock (X - 1, Y, Z)) != null) && n.CheckBlock (end))
			res.Add (n);
		if (((n = World.getBlock (X, Y, Z + 1)) != null) && n.CheckBlock (end))
			res.Add (n);
		if (((n = World.getBlock (X, Y, Z - 1)) != null) && n.CheckBlock (end))
			res.Add (n);
		res.Sort (
			delegate(Block p1, Block p2) {
				return -p1.DistanceToBlock (end).CompareTo (-p2.DistanceToBlock (end));
			}
		);
		return res;
	}

	public virtual int[] GetTile (Direction dir)
	{
		return new int[2] { 2, 0 };
	}

	public virtual Vector2[] StatusUV ()
	{
		Vector2[] uvs = new Vector2[4];
		int[] tex = { BlockStatus % 4, BlockStatus / 4 };
		uvs [0] = new Vector2 (TileSize * tex [0] + TileSize, TileSize * tex [1]);
		uvs [1] = new Vector2 (TileSize * tex [0] + TileSize, TileSize * tex [1] + TileSize);
		uvs [2] = new Vector2 (TileSize * tex [0], TileSize * tex [1] + TileSize);
		uvs [3] = new Vector2 (TileSize * tex [0], TileSize * tex [1]);
		return uvs;
	}

	public virtual Vector2[] GetUV (Direction dir)
	{
		Vector2[] uvs = new Vector2[4];
		int[] tex = GetTile (dir);
		uvs [0] = new Vector2 (TileSize * tex [0] + TileSize, TileSize * tex [1]);
		uvs [1] = new Vector2 (TileSize * tex [0] + TileSize, TileSize * tex [1] + TileSize);
		uvs [2] = new Vector2 (TileSize * tex [0], TileSize * tex [1] + TileSize);
		uvs [3] = new Vector2 (TileSize * tex [0], TileSize * tex [1]);
		return uvs;
	}

	public virtual MeshData Render (MeshData md)
	{
		if (!(Z + 1 < World.sizez) || !World.getBlock (X, Y, Z + 1).IsSolid (Direction.south)) {
			md = FaceDataNorth (md, 1, false);
			md.uv.AddRange (GetUV (Direction.north));
			//			md = FaceDataNorth (md,1,1,true);
			//			md.uv.AddRange (getUV (Direction.south));
			//			if(bStatus != -1)
			//				md = FaceDataNorth (md,1.0001f,2,false);
		}
		if (!(X + 1 < World.sizex) || !World.getBlock (X + 1, Y, Z).IsSolid (Direction.west)) {
			md = FaceDataEast (md, 1, false);
			md.uv.AddRange (GetUV (Direction.east));
			//			md = FaceDataEast (md, 1, 1, true);
			//			if (bStatus != -1)
			//				md = FaceDataEast (md, 1.0001f, 2, false);
		}
		if (!(Z - 1 > 0) || !World.getBlock (X, Y, Z - 1).IsSolid (Direction.north)) {
			md = FaceDataSouth (md, 1, false);
			md.uv.AddRange (GetUV (Direction.south));
			//			md = FaceDataSouth (md, 1, 1, true);
			//			if (bStatus != -1)
			//				md = FaceDataSouth (md, 1.0001f, 2, false);
		}
		if (!(X - 1 > 0) || !World.getBlock (X - 1, Y, Z).IsSolid (Direction.east)) {
			md = FaceDataWest (md, 1, false);
			md.uv.AddRange (GetUV (Direction.west));
			//			md = FaceDataWest (md, 1, 1, true);
			//			if (bStatus != -1)
			//				md = FaceDataWest (md, 1.0001f, 2, false);
		}
		if (!(Y + 1 < World.sizey) || !World.getBlock (X, Y + 1, Z).IsSolid (Direction.down)) {
			md = FaceDataUp (md, 1, false);
			md.uv.AddRange (GetUV (Direction.up));
			//			if (bStatus != -1)
			//				md = FaceDataUp (md, 1.0001f, 2, false);
		}
		if (!(Y - 1 > 0) || !World.getBlock (X, Y - 1, Z).IsSolid (Direction.up)) {
			md = FaceDataDown (md, 1, false);
			md.uv.AddRange (GetUV (Direction.down));
			//			if (bStatus != -1)
			//				md = FaceDataDown (md, 1.0001f, 2, false);
		}
		return md;
	}

	public virtual MeshData MeshBack (MeshData md)
	{
		if (!(Z + 1 < World.sizez) || !World.getBlock (X, Y, Z + 1).IsSolid (Direction.south)) {
			md = FaceDataNorth (md, 1, true);
			md.uv.AddRange (GetUV (Direction.south));
		}
		if (!(X + 1 < World.sizex) || !World.getBlock (X + 1, Y, Z).IsSolid (Direction.west)) {
			md = FaceDataEast (md, 1, true);
			md.uv.AddRange (GetUV (Direction.west));
		}
		if (!(Z - 1 > 0) || !World.getBlock (X, Y, Z - 1).IsSolid (Direction.north)) {
			md = FaceDataSouth (md, 1, true);
			md.uv.AddRange (GetUV (Direction.north));
		}
		if (!(X - 1 > 0) || !World.getBlock (X - 1, Y, Z).IsSolid (Direction.east)) {
			md = FaceDataWest (md, 1, true);
			md.uv.AddRange (GetUV (Direction.east));
		}
		//		if (!(y + 1 < World.sizey) || !World.getBlock (x, y + 1, z).isSolid (Direction.down)) {
		//			md = FaceDataUp (md, 1, true);
		//			md.uv.AddRange (getUV (Direction.down));
		//		}
		//		if (!(y - 1 > 0) || !World.getBlock (x, y - 1, z).isSolid (Direction.up)) {
		//			md = FaceDataDown (md, 1, true);
		//			md.uv.AddRange (getUV (Direction.up));
		//		}
		return md;
	}

	public virtual MeshData MeshFront (MeshData md)
	{
		if (!(Z + 1 < World.sizez) || !World.getBlock (X, Y, Z + 1).IsSolid (Direction.south)) {
			md = FaceDataNorth (md, 1, false);
			md.uv.AddRange (GetUV (Direction.north));
		}
		if (!(X + 1 < World.sizex) || !World.getBlock (X + 1, Y, Z).IsSolid (Direction.west)) {
			md = FaceDataEast (md, 1, false);
			md.uv.AddRange (GetUV (Direction.east));
		}
		if (!(Z - 1 > 0) || !World.getBlock (X, Y, Z - 1).IsSolid (Direction.north)) {
			md = FaceDataSouth (md, 1, false);
			md.uv.AddRange (GetUV (Direction.south));
		}
		if (!(X - 1 > 0) || !World.getBlock (X - 1, Y, Z).IsSolid (Direction.east)) {
			md = FaceDataWest (md, 1, false);
			md.uv.AddRange (GetUV (Direction.west));
		}
		if (!(Y + 1 < World.sizey) || !World.getBlock (X, Y + 1, Z).IsSolid (Direction.down)) {
			md = FaceDataUp (md, 1, false);
			md.uv.AddRange (GetUV (Direction.up));
		}
		if (!(Y - 1 > 0) || !World.getBlock (X, Y - 1, Z).IsSolid (Direction.up)) {
			md = FaceDataDown (md, 1, false);
			md.uv.AddRange (GetUV (Direction.down));
		}
		return md;
	}

	public virtual MeshData MeshStatus (MeshData md)
	{
		if (BlockStatus != -1) {
			//			Debug.LogFormat ("Rendering status for block {0},{1},{2} {3}",x,y,z,bStatus);
			if (!(Z + 1 < World.sizez) || !World.getBlock (X, Y, Z + 1).IsSolid (Direction.south)) {
				md = FaceDataNorth (md, 1.001f, false);
				md.uv.AddRange (StatusUV ());
			}
			if (!(X + 1 < World.sizex) || !World.getBlock (X + 1, Y, Z).IsSolid (Direction.west)) {
				md = FaceDataEast (md, 1.001f, false);
				md.uv.AddRange (StatusUV ());
			}
			if (!(Z - 1 > 0) || !World.getBlock (X, Y, Z - 1).IsSolid (Direction.north)) {
				md = FaceDataSouth (md, 1.001f, false);
				md.uv.AddRange (StatusUV ());
			}
			if (!(X - 1 > 0) || !World.getBlock (X - 1, Y, Z).IsSolid (Direction.east)) {
				md = FaceDataWest (md, 1.001f, false);
				md.uv.AddRange (StatusUV ());
			}
			if (!(Y + 1 < World.sizey) || !World.getBlock (X, Y + 1, Z).IsSolid (Direction.down)) {
				md = FaceDataUp (md, 1.001f, false);
				md.uv.AddRange (StatusUV ());
			}
			if (!(Y - 1 > 0) || !World.getBlock (X, Y - 1, Z).IsSolid (Direction.up)) {
				md = FaceDataDown (md, 1.001f, false);
				md.uv.AddRange (StatusUV ());
			}
		}
		return md;
	}
	


	//Generalize these functions?

	protected virtual MeshData FaceDataUp (MeshData md, float scale = 1, bool back = false)
	{
		md.vertices.Add (Corners [4] * scale + Position);
		md.vertices.Add (Corners [5] * scale + Position);
		md.vertices.Add (Corners [1] * scale + Position);
		md.vertices.Add (Corners [0] * scale + Position);
		md.AddQuadTriangles ();
//		md.uv.AddRange (getUV (Direction.up));
		return md;
	}

	protected virtual MeshData FaceDataDown (MeshData md, float scale = 1, bool back = false)
	{
		md.vertices.Add (Corners [3] * scale + Position);
		md.vertices.Add (Corners [2] * scale + Position);
		md.vertices.Add (Corners [6] * scale + Position);
		md.vertices.Add (Corners [7] * scale + Position);
		md.AddQuadTriangles ();
//		md.uv.AddRange (getUV (Direction.down));
		return md;
	}

	protected virtual MeshData FaceDataNorth (MeshData md, float scale = 1, bool back = false)
	{
		if (!back) {
			md.vertices.Add (Corners [6] * scale + Position);
			md.vertices.Add (Corners [5] * scale + Position);
			md.vertices.Add (Corners [4] * scale + Position);
			md.vertices.Add (Corners [7] * scale + Position);
			md.AddQuadTriangles ();
//			md.uv.AddRange (getUV (Direction.north));
		} else {
			md.vertices.Add (Corners [5] * scale + Position);
			md.vertices.Add (Corners [6] * scale + Position);
			md.vertices.Add (Corners [7] * scale + Position);
			md.vertices.Add (Corners [4] * scale + Position);
			md.AddQuadTriangles ();
//			md.uv.AddRange (getUV (Direction.south));
		}
		return md;
	}

	protected virtual MeshData FaceDataEast (MeshData md, float scale = 1, bool back = false)
	{
		if (!back) {
			md.vertices.Add (Corners [2] * scale + Position);
			md.vertices.Add (Corners [1] * scale + Position);
			md.vertices.Add (Corners [5] * scale + Position);
			md.vertices.Add (Corners [6] * scale + Position);
			md.AddQuadTriangles ();
//			md.uv.AddRange (getUV (Direction.east));
		} else {
			md.vertices.Add (Corners [1] * scale + Position);
			md.vertices.Add (Corners [2] * scale + Position);
			md.vertices.Add (Corners [6] * scale + Position);
			md.vertices.Add (Corners [5] * scale + Position);
			md.AddQuadTriangles ();
//			md.uv.AddRange (getUV (Direction.west));
		}
		return md;
	}

	protected virtual MeshData FaceDataSouth (MeshData md, float scale = 1, bool back = false)
	{
		if (!back) {
			md.vertices.Add (Corners [3] * scale + Position);
			md.vertices.Add (Corners [0] * scale + Position);
			md.vertices.Add (Corners [1] * scale + Position);
			md.vertices.Add (Corners [2] * scale + Position);
			md.AddQuadTriangles ();
//			md.uv.AddRange (getUV (Direction.south));
		} else {
			md.vertices.Add (Corners [0] * scale + Position);
			md.vertices.Add (Corners [3] * scale + Position);
			md.vertices.Add (Corners [2] * scale + Position);
			md.vertices.Add (Corners [1] * scale + Position);
			md.AddQuadTriangles ();
//			md.uv.AddRange (getUV (Direction.north));
		}
		return md;
	}

	protected virtual MeshData FaceDataWest (MeshData md, float scale = 1, bool back = false)
	{
		if (!back) {
			md.vertices.Add (Corners [7] * scale + Position);
			md.vertices.Add (Corners [4] * scale + Position);
			md.vertices.Add (Corners [0] * scale + Position);
			md.vertices.Add (Corners [3] * scale + Position);
			md.AddQuadTriangles ();
//			md.uv.AddRange (getUV (Direction.west));
		} else {
			md.vertices.Add (Corners [4] * scale + Position);
			md.vertices.Add (Corners [7] * scale + Position);
			md.vertices.Add (Corners [3] * scale + Position);
			md.vertices.Add (Corners [0] * scale + Position);
			md.AddQuadTriangles ();
//			md.uv.AddRange (getUV (Direction.east));
		}
		return md;
	}
}