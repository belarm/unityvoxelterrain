using UnityEngine;
using System.Collections;

public class BlockBrick : Block {
	public BlockBrick(int px, int py, int pz) : base(px, py, pz){
	}
	public BlockBrick() : base(){
	}
	
	/*public override MeshData render (MeshData md)
	{
		return md;
	}*/
	
	/*public override bool isSolid (Direction dir)
	{
		return false;
	}
	
	public override bool isBlocking(){
		//beenchecked = true;
		return false;
	}*/

	public override int[] GetTile(Direction dir){
		return new int[2] {3, 1};
	}
}
