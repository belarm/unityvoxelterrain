using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public struct NodePos
{
	public int X;
	public int Y;
	public int Z;

	public NodePos (PathNode node)
	{
		this.X = node.X;
		this.Y = node.Y;
		this.Z = node.Z;
	}
	public NodePos (int _x, int _y, int _z) {
		X = _x;
		Y = _y;
		Z = _z;
	}
}


public static class Moves
{
	public const byte N = 0;
	public const byte NE = 1;
	public const byte E = 2;
	public const byte SE = 3;
	public const byte S = 4;
	public const byte SW = 5;
	public const byte W = 6;
	public const byte NW = 7;
	public static byte[,] calc = new byte[,] { { 5, 6, 7 }, { 4, 127, 0 }, { 3, 2, 1 } };
	public static Vector3[] m2v = {
		Vector3.forward,
		Vector3.forward + Vector3.right,
		Vector3.right,
		Vector3.right + Vector3.back,
		Vector3.back,
		Vector3.back + Vector3.left,
		Vector3.left,
		Vector3.left + Vector3.forward
	};
}


public class PathEdge : IEquatable<PathEdge>
{
	#region members

	public PathNode Start;
	public PathNode End;
	public bool IsInterLeaf = false;
	public const bool IsVirtual = false;
	public int Level = 0;
	public List<byte> PathMoves;
	public static Color[] LevelColors = {
		Color.magenta,
		Color.red,
		Color.green,
		Color.white,
		Color.yellow,
		Color.magenta,
		Color.green,
		Color.gray
	};

	protected double EdgeCost;
	public const int HASHSIZE = 65536 * 256 + 1;
	public const int Prime1 = 73856093;
	public const int Prime2 = 19349663;
	public const int Prime3 = 83492791;

	#endregion

	public PathEdge (PathNode _start, PathNode _end, int _level = 0, bool _isInterLeaf = false, double _cost = -Mathf.Infinity)
	{
		Start = _start;
		End = _end;
		IsInterLeaf = _isInterLeaf;
		EdgeCost = _cost <= -Mathf.Infinity ? Start.DistanceTo (End) : _cost;
		PathMoves = GetMoves (Start, End);
		//Level = _level;

		//TODO: I'm dubious
		Level = World.qTree [Start.Y].Top.FindParent (Start.Address, End.Address).Level;
		//PathDB.Nodes [Start.Y].Find(ref temp);
		Start.Level = Math.Max(Start.Level,Level);
		if (Start.Level < Level) {
			Start.Level = Level;
//			PathDB.Nodes [Start.Y].Update (Start);
		}
		//temp = PathDB.Nodes [End.Y] [End];
		if (End.Level < Level) {
			End.Level = Level;
//			PathDB.Nodes [End.Y].Update (End);
		}
	}

	protected PathEdge ()
	{
	}

	public virtual double Cost ()
	{
		return EdgeCost;
	}

	public override int GetHashCode ()
	{
		unchecked {
			int hash = 17;
			hash = hash * 31 + Start.GetHashCode ();
			hash = hash * 31 + End.GetHashCode ();
			return hash % HASHSIZE;
		}
	}

	bool IEquatable<PathEdge>.Equals (PathEdge other)
	{
		return (Start == other.Start && End == other.End);
	}


	public virtual PathEdge GetReverse ()
	{
		return new PathEdge (End, Start, Level, IsInterLeaf, EdgeCost);
	}

	#region Debug & Draw

	public virtual void Draw (int _level = 0)
	{
		//Debug.DrawLine (Start.Position + Vector3.up * _level, End.Position + Vector3.up * _level, Color.blue, 6f);
		Debug.DrawLine (Start.Position/* + Vector3.up * Level*/, End.Position/* + Vector3.up * Level*/, LevelColors [Level], 10f - Level);
	}

	public virtual void Draw (Color c, int _level = 0)
	{
		Debug.DrawLine (Start.Position + Vector3.down / 2 + Vector3.up * _level, End.Position + Vector3.down / 2 + Vector3.up * _level, c);
	}

	public override string ToString ()
	{
		return string.Format ("[PathEdge] ({0},{1},{2}) - ({3},{4},{5})", Start.X, Start.Y, Start.Z, End.X, End.Y, End.Z);
	}

	#endregion

	public static List<byte> GetMoves (PathNode a, PathNode b)
	{
		var moves = new List<byte> ();
		int dx, dy;
		var x = b.X - a.X;
		var y = b.Z - a.Z;
		while ((x != 0 || y != 0) && moves.Count < 100) { //Sanity check
			x -= dx = Math.Sign (x);
			y -= dy = Math.Sign (y);
			moves.Add (Moves.calc [dx + 1, dy + 1]);
		}
		return moves;
	}
}

public class Path: PathEdge
{
	public List<PathNode> Nodes;
	public List<PathEdge> Edges;
	public static Dictionary<PathNode,List<PathEdge>> EdgeCache = new Dictionary<PathNode, List<PathEdge>> ();
	public new const bool IsVirtual = true;

	public Path (PathNode _start, PathNode _end, List<PathEdge> _edges = null, int _level = -1)
	{
		Nodes = new List<PathNode> ();
		Level = _level == -1 ? Morton.FindLCR (_start.Address, _end.Address) : _level;
		Nodes.Add (_start);
		if ((Edges = _edges) == null) {
			Edges = new List<PathEdge> ();
			Edges.Add (new PathEdge (_start, _end));
		}
		foreach (PathEdge next in Edges) {
			Nodes.Add (next.End);
		}
		Start = _start;
		End = _end;
		EdgeCost = Edges [0].Cost ();
	}

	private Path (Path p)
	{
		Start = p.Start;
		End = p.End;
		Nodes = new List<PathNode> (p.Nodes);
		Edges = new List<PathEdge> (p.Edges);
		EdgeCost = p.Cost ();
		Level = p.Level;
	}

	public Path (PathEdge p)
	{
		Start = p.Start;
		End = p.End;
		Nodes = new List<PathNode> ();
		Edges = new List<PathEdge> ();
		Nodes.Add (Start);
		Nodes.Add (End);
		Edges.Add (p);
		Level = p.Level;
	}

	public Path (List<PathNode> nodes, int _level = 0)
	{
		Nodes = new List<PathNode> ();
		Edges = new List<PathEdge> ();
		Start = nodes [0];
		End = nodes [nodes.Count - 1];
		PathNode temp = null;
		PathNode last;
		PathEdge foundEdge;
		Level = _level;
		foreach (PathNode next in nodes) {
			last = temp;
			temp = next;
			Nodes.Add (temp);
			if (last != null) {
				foundEdge = PathDB.FindEdge (last, temp);
				if (foundEdge != null)
					Edges.Add (foundEdge);
			}
		}
	}

	public override double Cost ()
	{
		EdgeCost = 0;
		foreach (PathEdge edge in Edges) {
			EdgeCost += edge.Cost ();
		}
		return EdgeCost;
	}

	//	public static PathEdge FindPathEdge (PathNode _start, PathNode _end)
	//	{
	//		List<PathEdge> neighbors = EdgeCache [_start];
	//		foreach (PathEdge next in neighbors)
	//			if (next.End == _end)
	//				return next;
	//		return null;
	//	}

	public void Add (PathEdge edge)
	{
		Edges.Add (edge);
		Nodes.Add (edge.End);
		End = edge.End;
	}

	public void DelFront ()
	{
		Nodes.RemoveAt (0);
		Edges.RemoveAt (0);
		Start = Nodes [0];
	}

	public void DelEnd ()
	{
		Nodes.RemoveAt (Nodes.Count - 1);
		Edges.RemoveAt (Edges.Count - 1);
		End = Nodes [Nodes.Count - 1];
	}


	public override PathEdge GetReverse ()
	{
		Path ret = new Path (this);
		PathNode temp = ret.Start;
		ret.Start = ret.End;
		ret.End = temp;
		ret.Nodes.Reverse ();
		ret.Edges.Reverse ();
		for (int i = 0; i < ret.Edges.Count; i++) {
			ret.Edges [i] = ret.Edges [i].GetReverse ();
		}
		return ret;
	}

	public override void Draw (int _level = 0)
	{
		foreach (PathEdge next in Edges) {
			next.Draw (Level);
		}
	}


}

public class PathNode : IComparable
{

	const double Sqrt2 = 1.41421356237;
	public readonly int X, Y, Z;
	public Vector3 Position;
	public QTNode Parent;
	public int Level;

	public int Address {
		get {
			return Morton.Encode (X, Z);
		}
	}

	public PathNode (int sx, int sy, int sz)
	{
		X = sx;
		Y = sy;
		Z = sz;
		Position = new Vector3 (X, Y, Z);
		Parent = World.gWorld.FindQuad (Morton.Encode (X, Z), Y);
//		makeBall ();
	}

	public PathNode (Vector3 pos)
	{
		Position = pos;
		X = (int)Position.x;
		Y = (int)Position.y;
		Z = (int)Position.z;
		Parent = World.gWorld.FindQuad (Morton.Encode (X, Z), Y);
//		makeBall ();
	}

	public PathNode (int _x, int _z, QTNode _parent)
	{
		X = _x;
		Y = _parent.Tree.Layer;
		Z = _z;
		Parent = _parent;
		Position = new Vector3 (X, Y, Z);
//		makeBall ();
	}

	public PathNode (Vector3 pos, QTNode _parent)
	{
		Position = pos;
		X = (int)Position.x;
		Y = (int)Position.y;
		Z = (int)Position.z;
		Parent = _parent;
//		makeBall ();
	}

	public PathNode (Block b) : this (b.Position)
	{
	}

	public NodePos GetPos ()
	{
		NodePos np;
		np.X = X;
		np.Y = Y;
		np.Z = Z;
		return np;
	}

	public List<PathEdge> GetNeighbors (PathNode end)
	{
		List<PathEdge> ret = Path.EdgeCache [this];
		ret.Sort (
			(a, b) => a.End.H (end).CompareTo (b.End.H (end)));
		return ret;
	}

	public double H (PathNode end)
	{
		int dx = Math.Abs (X - end.X);
		int dy = Math.Abs (Y - end.Y);
		int min = Math.Min (dx, dy);
		int max = Math.Max (dx, dy);
		return Sqrt2 * min + max - min;
	}

	public bool SharesSideWith (PathNode cmp)
	{
		return (X == cmp.X) || (Y == cmp.Y);
	}

	//	bool IEquatable<PathNode>.Equals (PathNode cmp)
	//	{
	//		return (X == cmp.X && Y == cmp.Y && Z == cmp.Z);
	//	}

	//	public new static bool Equals (object a, object b)
	//	{
	//		if (ReferenceEquals (a, null) || ReferenceEquals (b, null))
	//			return false;
	//		if (!(a is PathNode) || !(b is PathNode))
	//			return false;
	//		if (ReferenceEquals (a, b))
	//			return true;
	//		return ((a as PathNode).X == (b as PathNode).X && (a as PathNode).Y == (b as PathNode).Y && (a as PathNode).Z == (b as PathNode).Z);
	//	}

	public override bool Equals (object obj)
	{
		if (ReferenceEquals (null, obj))
			return false;
		if (ReferenceEquals (this, obj))
			return true;
		if (obj.GetType () != GetType ())
			return false;
		//return Equals ((PathNode)obj);
		return this == obj as PathNode;
	}

	public static bool operator == (PathNode a, PathNode b)
	{
		return (a.X == b.X && a.Y == b.Y && a.Z == b.Z);
	}

	public static bool operator != (PathNode a, PathNode b)
	{
		return !(a == b);
	}

	public int CompareTo (PathNode a)
	{
		return Y == a.Y ? X == a.X ? Z.CompareTo (a.Z) : X.CompareTo (a.X) : Y.CompareTo (a.Y);
	}

	public override int GetHashCode ()
	{
		unchecked {
			int hash = 17;
			hash = hash * 31 + X;
			hash = hash * 31 + Y;
			hash = hash * 31 + Z;
			return hash % PathEdge.HASHSIZE;
		}
	}

	public float DistanceTo (PathNode target)
	{
		return Vector3.Distance (Position, target.Position);
	}

	public int CompareTo (object obj)
	{
		PathNode t = obj as PathNode;
		return GetHashCode ().CompareTo (t.GetHashCode ());
	}

	public override string ToString ()
	{
		return string.Format ("[PathNode] ({0},{1},{2})-{3}", X, Y, Z, Level);
	}

	#region Debug Functions

	public void makeBall ()
	{
		GameObject marker = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		marker.transform.localScale = new Vector3 (0.25f, 0.25f, 0.25f);
		marker.transform.position = Position;
		GameObject.Destroy (marker, 3);
//		Debug.LogFormat ("Made Ball at {0}", Position);
	}


	#endregion
}

