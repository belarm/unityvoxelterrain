﻿using System;
using UnityEngine;

//using System.Collections;
using System.Collections.Generic;

////using System.IO;
//using C5;
//using SCG = System.Collections.Generic;

//using UnityEngine.Assertions;


/// <summary>
/// Enum for Quadtree directions
/// </summary>
public static class QDir
{
	public const int SW = 0;
	public const int SE = 1;
	public const int NW = 2;
	public const int NE = 3;
}

/// <summary>
/// Enum of Ordinal movement values
/// </summary>
public static class QOrd
{
	public const int North = 0;
	public const int South = 1;
	public const int East = 2;
	public const int West = 3;
}




///Container class for QTNode. Implements wrappers for recursive QTNode methods
public class Quadtree
{
	///The top QTNode of the tree
	public QTNode Top;
	///The Y position of this tree
	public readonly int Layer;

	/// <summary>
	/// Initializes a new instance of the <see cref="Quadtree"/> class and its Top node
	/// </summary>
	/// <param name="_layer">The Y position of the tree</param>
	public Quadtree (int _layer)
	{
		Layer = _layer;
		Top = new QTNode (0, 0, QDir.NW, null, this, (int)Math.Log (World.sizex, 2));
	}

	///<summary> Wrapper for <see cref="QTNode.AddNode"/></summary>
	public QTNode AddNode (int addr, int depth)
	{
		return Top.AddNode (addr, depth);
	}

	public void WriteDOT ()
	{
		World.DOTFile.WriteLine ("graph QuadTreeLayer{0} {{", Layer);
		Top.WriteDOT ();
		World.DOTFile.WriteLine ("}");
	}
}

/// <summary>
/// Quadtree Node
/// </summary>
public class QTNode : IComparable
{
	#region members

	const double Sqrt2 = 1.41421356237;
	public readonly int X, Y;
	public readonly int Address;
	public readonly QTNode Parent;
	public readonly Quadtree Tree;
	public readonly int Level;
	public QTNode[] Children = new QTNode[4];
	public int NumChildren;
	public List<QTNode> Neighbors;
	public PathNode[] Corners = new PathNode[4];
	public List<PathNode> Portals = new List<PathNode> ();
	//public HashDictionary<PathNode,List<PathEdge>> PortalEdges = new HashDictionary<PathNode, List<PathEdge>> ();
	static public bool[,] Ords = {
		{ false, false, true, true },
		{ true, true, false, false },
		{ false, true, false, true },
		{ true, false, true, false }
	};

	#endregion

	#region Constructor

	public QTNode (int _x, int _y, int _address, QTNode _parent, Quadtree _tree, int _level)
	{
		X = _x;
		Y = _y;
		Address = _address;
		Parent = _parent;
		Tree = _tree;
		Level = _level;
		if (Level > 0) {
			Corners [QDir.SW] = new PathNode (X, Y, this);
			Corners [QDir.SE] = new PathNode (X + Size () - 1, Y, this);
			Corners [QDir.NW] = new PathNode (X, Y + Size () - 1, this);
			Corners [QDir.NE] = new PathNode (X + Size () - 1, Y + Size () - 1, this);
		} else {
			PathNode pn = new PathNode (X, Y, this);
			Corners [QDir.SW] = pn;
			Corners [QDir.SE] = pn;
			Corners [QDir.NW] = pn;
			Corners [QDir.NE] = pn;
		}
//		Neighbors = new List<QTNode> ();
	}

	#endregion

	#region Tree manipulation

	public QTNode AddHole (int addr, int depth = 0)
	{
		int cPos = Morton.CalcDir (addr, Level - 1);
		QTNode qt = Children [cPos];
		if ((qt != null) && (qt.Level > 0) && (qt.IsLeaf ())) {
			qt.Fill ();
			qt = Children [cPos];
		}
		if (Level > 1) {
			if (qt == null)
				qt = CreateChild (cPos);
			return qt.AddHole (addr, depth);
		} else {
			if ((qt = Children [cPos]) != null)
				qt.Delete ();
			if (IsLeaf ())
				Prune ();
			return this;
		}
	}

	public QTNode AddNode (int addr, int depth = 0)
	{
		if (Level == depth) {
			return this;
		}
		int cpos = Morton.CalcDir (addr, Level - 1);
		QTNode cn = CreateChild (cpos), ret;
		ret = cn.AddNode (addr, depth);
		bool clear = true;
		if (NumChildren == 4) {
			for (int child = 0; child < 4; child++) {
				clear &= Children [child].IsLeaf ();
			}
			if (clear) {
				NumChildren = 0;
				//Tree.Count -= 4;
				Children = new QTNode[4];
				return this;
			}
		}
		return ret;
	}


	public QTNode CreateChild (int p)
	{
		if (Children [p] != null)
			return Children [p];
		NumChildren++;
		int size = Size (this);
		switch (p) {
		case (QDir.SW):
			return Children [QDir.SW] = new QTNode (X, Y, QDir.SW, this, Tree, Level - 1);
		case (QDir.SE):
			return Children [QDir.SE] = new QTNode (X + size / 2, Y, QDir.SE, this, Tree, Level - 1);
		case (QDir.NW):
			return Children [QDir.NW] = new QTNode (X, Y + size / 2, QDir.NW, this, Tree, Level - 1);
		case (QDir.NE):
			return Children [QDir.NE] = new QTNode (X + size / 2, Y + size / 2, QDir.NE, this, Tree, Level - 1);
		default:
			return null;
		}
	}

	public void Delete ()
	{
		Parent.NumChildren--;
		Parent.Children [Address & 3] = null;
		//parent.Prune ();
	}

	public void Fill ()
	{
		for (int i = 0; i < 4; i++)
			if (Children [i] == null)
				CreateChild (i);
	}

	public void Prune ()
	{
		if (IsLeaf ())
			Delete ();
		if (Level != World.maxDepth)
			Parent.Prune ();
	}

	#endregion Tree manipulation

	#region Searches

	public QTNode FindNodeXY (int tx, int ty, int depth = 0)
	{
		return FindNode (Morton.Encode (tx, ty), depth);
	}

	public QTNode FindNode (int addr, int depth = 0)
	{
		if (((addr == Address) && (Level == 0)) || (Level == depth) || IsLeaf ())
			return this;
		int cpos = Morton.CalcDir (addr, Level - 1);
		return Children [cpos] == null ? null : Children [cpos].FindNode (addr, depth);
	}

	public QTNode FindParent (int a, int b)
	{
		if (IsLeaf ())
			return this;
		int adir = Morton.CalcDir (a, Level - 1);
		int bdir = Morton.CalcDir (b, Level - 1);
		if (adir != bdir)
			return this;
		return Children [adir] == null ? null : Children [adir].FindParent (a, b);
	}

	#endregion Searches

	#region Tests

	public bool CanGo (int ordinal)
	{
		switch (ordinal) {
		case QOrd.North:
			return Y + Size (this) < World.sizez;
		case QOrd.South:
			return Y > 0;
		case QOrd.West:
			return X > 0;
		case QOrd.East:
			return X + Size (this) < World.sizex;
		}
		return false;
	}

	public bool Contains (int _address)
	{
		//		int x, y;
		//		Morton.Decode (_address, out x, out y);
		return ((_address >> (Level * 2)) ^ (Address)) == 0;
		//		return Contains (x, y);
	}

	public bool IsLeaf ()
	{
		return NumChildren == 0;
	}


	public bool OnInnerEdge (PathNode node)
	{
		return Corners [Address & 3].SharesSideWith (node);
	}

	public bool OnOuterEdge (PathNode node)
	{
		return Corners [(Address & 3) ^ 3].SharesSideWith (node);
	}


	#endregion Tests

	#region Pathing

	public PathEdge AddPathEdge (PathEdge _edge)
	{
		//		List<PathEdge> edges;
		if (!_edge.Start.Equals (null))
			PathDB.AddEdge (_edge);
		//			PortalEdges [_edge.Start].Add (_edge);
		else {
			//			Debug.Log ("WTF");
		}

		return null;
	}

	//	public List<Path> CachePaths ()
	//	{
	//		if (!IsLeaf ()) {
	//			var cornerNodes = new List<PathNode>[4];
	//			for (int i = 0; i < 4; i++) {
	//				if (Children [i] != null) {
	//					Children [i].CachePaths ();
	//					cornerNodes [i] = Children [i].OuterPortals ();
	//				}
	//			}
	//			for (int i = 0; i < 4; i++)
	//				for (int j = i + 1; j < 4; j++) {
	//					if ((Children [i] != null) && (Children [j] != null)) {
	//						foreach (PathNode start in cornerNodes[i]) {
	//							foreach (PathNode end in cornerNodes[j]) {
	//								var newedge = PathFind.Search (start, end, this);
	//								if (newedge != null)
	//									PathDB.AddEdge (newedge, Level);
	//							}
	//						}
	//					}
	//					//					var newedge = PathFind.Search (Portals [i], Portals [j]);
	//					//					if (newedge != null)
	//					//						PathDB.AddEdge (PathFind.Search (Portals [i], Portals [j]));
	//				}
	//		} else {
	//			for (int i = 0; i < Portals.Count; i++)
	//				for (int j = i + 1; j < Portals.Count; j++)
	//					PathDB.AddEdge (Portals [i], Portals [j], Level);
	//			//			foreach (var pstart in Portals) {
	//			//				int index = Portals.IndexOf (pstart);
	//			//				if (index < Portals.Count - 1) {
	//			//					foreach (var pend in Portals.GetRange(index + 1,Portals.Count - index - 1)) {
	//			//						//if (pstart != pend) {
	//			//						//	AddPathEdge (new PathEdge (pstart, pend));
	//			//						PathDB.AddEdge (pstart, pend);
	//			//						//}
	//			//					}
	//			//				}
	//			//			}
	//		}
	//		return null;
	//	}

	public int H (QTNode end)
	{
		return (int)Math.Sqrt (Math.Pow (X - end.X, 2) + Math.Pow (Y - end.Y, 2));
	}

	public List<PathEdge> InterEdges (PathNode node)
	{
		List<PathEdge> ret = new List<PathEdge> ();
		List<PathEdge> edges;
		PathDB.Edges [Tree.Layer].TryGetValue (node.GetPos(), out edges);
		if (edges != null) {
			foreach (PathEdge edge in edges) {
				if (OnOuterEdge (edge.End) && (edge.Level >= Level))
					ret.Add (edge);
			}
		}
		return ret;
	}

	public List<PathEdge> IntraEdges (PathNode node)
	{
		List<PathEdge> ret = new List<PathEdge> ();
		List<PathEdge> edges;
		PathDB.Edges [Tree.Layer].TryGetValue (node.GetPos(), out edges);
		if (edges != null) {
			foreach (PathEdge edge in edges) {
				if (OnInnerEdge (edge.End))
					ret.Add (edge);
			}
		}
		return ret;
	}

	public bool LinkNeighbors ()
	{
		if (!IsLeaf ()) {
			Portals = new List<PathNode> ();
			for (int i = 0; i < 4; i++) {
				if (Children [i] != null) {
					Children [i].LinkNeighbors ();
					Portals.AddRange (Children [i].OuterPortals ());
				}
			}
		} else {
			int x1 = X;
			int x2 = X + Size () - 1;
			int y1 = Y;
			int y2 = Y + Size () - 1;
			if (CanGo (QOrd.North)) {
				//North from NW Corner
				MakeLink (x1, y2 + 1, QDir.NW);
				//North from NE Corner
				MakeLink (x2, y2 + 1, QDir.NE);
			}
			if (CanGo (QOrd.South)) {
				//South from SW Corner
				MakeLink (x1, y1 - 1, QDir.SW);
				//South from SE Corner
				MakeLink (x2, y1 - 1, QDir.SE);
			}
			if (CanGo (QOrd.East)) {
				//East from NE Corner
				MakeLink (x2 + 1, y2, QDir.NE);
				//East from SE Corner
				MakeLink (x2 + 1, y1, QDir.SE);
			}
			if (CanGo (QOrd.West)) {
				//West from NW Corner
				MakeLink (x1 - 1, y2, QDir.NW);
				//West from SW Corner
				MakeLink (x1 - 1, y1, QDir.SW);
			}
			if (CanGo (QOrd.South) && CanGo (QOrd.West))
				MakeLink (x1 - 1, y1 - 1, QDir.SW);
			if (CanGo (QOrd.South) && CanGo (QOrd.East))
				MakeLink (x2 + 1, y1 - 1, QDir.SE);
			if (CanGo (QOrd.North) && CanGo (QOrd.West))
				MakeLink (x1 - 1, y2 + 1, QDir.NW);
			if (CanGo (QOrd.North) && CanGo (QOrd.East))
				MakeLink (x2 + 1, y2 + 1, QDir.NE);
		}

		return false;
	}

	void MakeLink (int _x, int _y, int _dir)
	{
		//		Debug.LogFormat ("Making Link from {0},{1},{2}-{3},{1},{4}", X, Tree.Layer, Y, _x, _y);
		if (!World.blocks [_x, Tree.Layer, _y].IsBlocking ()) {

			QTNode neighbor;
			PathNode pnFar, pnNear;
			PathEdge linkEdge;
			neighbor = Tree.Top.FindNodeXY (_x, _y);
			if ((neighbor != null)) {
				pnFar = PathDB.AddNode (_x, _y, neighbor);
			
				pnNear = Corners [_dir];
				PathDB.FindOrAddNode (ref pnNear);
				PathDB.FindOrAddNode (ref pnFar);
				if (!neighbor.Portals.Contains (pnFar)) {
					neighbor.Portals.Add (pnFar);
				}
				//pnNear = AddPathNode (Corners [_dir].X, Corners [_dir].Y);
				linkEdge = new PathEdge (pnNear, pnFar, 0, true, 1);
				pnFar.Level = Math.Max (pnFar.Level, linkEdge.Level);
				pnNear.Level = Math.Max (pnNear.Level, linkEdge.Level);
				AddPathEdge (linkEdge);
				//neighbor.AddPathEdge (linkEdge.GetReverse ());
			}
		}
	}

	public List<PathNode> OuterPortals ()
	{
		var ret = new List<PathNode> ();
		bool keep;
		foreach (PathNode node in Portals) {
			if (node.Level > Level)
				ret.Add (node);
			//keep = false;
			//foreach (PathEdge path in PathDB.FindEdges(node,0)) {
			//	keep |= path.IsInterLeaf;
			//}
			//if (!keep)
			//	ret.Remove (node);
		}

		//		var ret = new List<PathNode> (Portals);
		//		int xval = 5, yval = 5;
		//		switch (Address & 3) {
		//		case QDir.NE:
		//			xval = X + Size () - 1;
		//			yval = Y + Size () - 1;
		//			break;
		//		case QDir.NW:
		//			xval = X;
		//			yval = Y + Size () - 1;
		//			break;
		//		case QDir.SE:
		//			xval = X + Size () - 1;
		//			yval = Y;
		//			break;
		//		case QDir.SW:
		//			xval = X;
		//			yval = Y;
		//			break;
		//		}
		//		foreach (PathNode next in Portals) {
		//			if ((next.X != xval) && (next.Y != yval)) {
		//				ret.Remove (next);
		//			}
		//		}
		//		var ret = new C5.HashSet<PathNode> ();
		//		foreach (PathNode next in Portals) {
		//			if (next.Level > Level)
		//				ret.Add (next);
		//		}
		return ret;
	}

	#endregion Pathing



	//	public PathNode AddPathNode (int _x, int _y)
	//	{
	//		if ((_x < X) || (_x >= X + Size ()) || (_y < Y) || (_y >= Y + Size ())) {
	//			Debug.Log ("Bad Insertion!");
	//		}
	//		PathNode pnode;
	//		pnode = new PathNode (_x, _y, this);
	//		if (!Portals.Contains (pnode))
	//			Portals.Add (pnode);
	//		return pnode;
	//	}





	//	public bool Contains (int _x, int _y)
	//	{
	//		if ((_x < X) || (_y < Y) || (_x > X + Size () - 1) || (_y > Y + Size () - 1))
	//			return false;
	//		return true;
	//	}






	//	public List<QTNode> GetSide (int ordinal)
	//	{
	//		var ret = new List<QTNode> ();
	//		for (int i = 0; i < 3; i++) {
	//			if (Ords [ordinal, i] && (Children [i] != null)) {
	//				if (Children [i].IsLeaf ()) {
	//					ret.Add (Children [i]);
	//				} else {
	//					ret.AddRange (Children [i].GetSide (ordinal));
	//				}
	//			}
	//		}
	//		return ret;
	//	}



	//	public List<PathEdge> OuterEdges (PathNode node)
	//	{
	//		List<PathEdge> ret = new List<PathEdge> ();
	//		foreach (PathEdge edge in PathDB.FindEdges (node,Level)) {
	//			if (OnOuterEdge (edge.End) && (edge.Level > Level))
	//				ret.Add (edge);
	//		}
	//		return ret;
	//	}

	/// <summary>
	/// Get 'outer' portals - those with paths exiting the parent quad
	/// </summary>
	/// <returns>The outer portals.</returns>




	#region interface methods
	//TODO:This will not work
	public int CompareTo (object obj)
	{
		if (obj == null)
			throw new ArgumentNullException ("obj");
		return Address.CompareTo ((obj as QTNode).Address);
	}

	public override bool Equals (object obj)
	{
		return Address.Equals ((obj as QTNode).Address);
	}

	public override int GetHashCode ()
	{
		int hash = 17;
		hash = hash * 31 + Address.GetHashCode ();
		hash = hash * 31 + Level.GetHashCode ();
		hash = hash * 31 + Tree.Layer.GetHashCode ();
		return hash;
	}

	#endregion

	#region Static methods

	public static int Size (QTNode qtn)
	{
		return 1 << qtn.Level;
	}

	public int Size ()
	{
		return 1 << Level;
	}

	public static double DiagDist (int[] a, int[] b)
	{
		int dx = Math.Abs (a [0] - b [0]);
		int dy = Math.Abs (a [1] - b [1]);
		int min = Math.Min (dx, dy);
		int max = Math.Max (dx, dy);
		return Sqrt2 * min + max - min;
	}

	#endregion

	//	public static

	#region Debug

	public void WriteDOT ()
	{
		foreach (QTNode child in Children) {
			if (child != null) {
				World.DOTFile.WriteLine ("QT{0}_{1}_{2} -- QT{3}_{4}_{5}", X, Y, Level, child.X, child.Y, child.Level);
				child.WriteDOT ();
			}
		}
	}

	public void DrawIfEmpty ()
	{
		if (IsLeaf ())
			DrawQuad ();
		else {
			if (Level > 0) {
				if (Children [QDir.NW] != null)
					Children [QDir.NW].DrawIfEmpty ();
				if (Children [QDir.NE] != null)
					Children [QDir.NE].DrawIfEmpty ();
				if (Children [QDir.SW] != null)
					Children [QDir.SW].DrawIfEmpty ();
				if (Children [QDir.SE] != null)
					Children [QDir.SE].DrawIfEmpty ();
			}
			DrawQuad ();
		}
//		foreach (PathNode n in Portals) {
//			Debug.DrawLine (new Vector3 (X + Size () / 2 - .5f, Tree.Layer + Level, Y + Size () / 2 - .5f), n.Position + Level * Vector3.up, Color.red, 3f);
//		}
	}

	public void DrawIfEmpty (int target)
	{
		if (IsLeaf ()) {
			DrawQuad ();
			if (Level > 0) {
				foreach (PathNode pn in Corners) {
					pn.makeBall ();
				}
			} else {
				Corners [QDir.NW].makeBall ();
			}
		} else {
			if (Level > target) {
				if (Children [QDir.NW] != null)
					Children [QDir.NW].DrawIfEmpty (target);
				if (Children [QDir.NE] != null)
					Children [QDir.NE].DrawIfEmpty (target);
				if (Children [QDir.SW] != null)
					Children [QDir.SW].DrawIfEmpty (target);
				if (Children [QDir.SE] != null)
					Children [QDir.SE].DrawIfEmpty (target);
			}

//			DrawQuad ();
		}
//		if (Level == target)
//			foreach (PathNode n in Portals) {
//				Debug.DrawLine (new Vector3 (X + Size () / 2 - .5f, Tree.Layer, Y + Size () / 2 - .5f), n.Position, Color.red, 3f);
//			}
	}

	public void DrawQuad ()
	{
		//Tree.DrawnCount++;
		var corners = new Vector3[4];
		int w, h;
		w = h = (int)Math.Pow (2, Level);
		var shade = IsLeaf () ? Color.red : Color.white;
		corners [QDir.NW] = new Vector3 (X - 0.5f, Tree.Layer - .5f, Y - 0.5f);
		corners [QDir.NE] = new Vector3 (X - 0.5f, Tree.Layer - .5f, Y + h - 0.5f);
		corners [QDir.SW] = new Vector3 (X + w - 0.5f, Tree.Layer - .5f, Y + h - 0.5f);
		corners [QDir.SE] = new Vector3 (X + w - 0.5f, Tree.Layer - .5f, Y - 0.5f);
		Debug.DrawLine (corners [0], corners [1], shade, 10f - Level);
		Debug.DrawLine (corners [1], corners [2], shade, 10f - Level);
		Debug.DrawLine (corners [2], corners [3], shade, 10f - Level);
		Debug.DrawLine (corners [3], corners [0], shade, 10f - Level);

//		for (int i = 0; i <= World.maxDepth; i++)
//			foreach (PathNode pn in Portals) {
//				List<PathEdge> edgelist = PathDB.FindEdges (pn, i);
//				foreach (PathEdge edge in edgelist) {
//					if (edge != null)
//						edge.Draw ();
//				}
//			}
	}

	public void FakeLink (int _x, int _y, int _dir)
	{
		//		Debug.LogFormat ("Making Link from {0},{1},{2}-{3},{1},{4}", X, Tree.Layer, Y, _x, _y);
		if (World.blocks [_x, Tree.Layer, _y].IsBlocking ()) {
			Debug.DrawLine (new Vector3 (_x, Tree.Layer, _y), Corners [_dir].Position, Color.yellow, 5f);
		} else {
			Debug.DrawLine (new Vector3 (_x, Tree.Layer, _y), Corners [_dir].Position, Color.cyan, 5f);
		}

	}

	#endregion

}
