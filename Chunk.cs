using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[RequireComponent(typeof(MeshFilter))]
//[RequireComponent(typeof(MeshRenderer))]
//[RequireComponent(typeof(MeshCollider))]

public class Chunk{

	//public bool render = false;
	public bool updated = true;
	public World pWorld;
	public int x,y,z;
	public MeshFilter frontFilter, backFilter, statusFilter;
	public MeshCollider coll;
	public MeshRenderer rendererFront, rendererBack, rendererStatus;
	public List<Block> portals;
//	public Path[,] Paths;
	private GameObject frontGO, backGO, statusGO;


	public Chunk (World wrld, int px, int py, int pz) {
		x = px;
		y = py;
		z = pz;
		pWorld = wrld;
		frontGO = new GameObject ();
		backGO = new GameObject ();
		statusGO = new GameObject ();
		frontFilter = frontGO.AddComponent<MeshFilter> ();
		coll = frontGO.AddComponent<MeshCollider> ();
		backFilter = backGO.AddComponent<MeshFilter> ();
		statusFilter = statusGO.AddComponent<MeshFilter> ();
		rendererFront = frontGO.AddComponent<MeshRenderer> ();
		rendererFront.material = pWorld.terrainMat[0];
		rendererFront.enabled = false;
		rendererBack = backGO.AddComponent<MeshRenderer> ();
		rendererBack.material = pWorld.terrainMat[1];
		rendererBack.enabled = false;
		rendererStatus = statusGO.AddComponent<MeshRenderer> ();
		rendererStatus.material = pWorld.terrainMat[2];
		rendererStatus.enabled = false;
		//Debug.Log (renderer.material);
	}
	
/*	public Block getBlock(int x, int y, int z) {
		return pWorld.blocks [x, y, z];
	}*/

	public bool disable(){
		rendererFront.enabled = false;
		rendererBack.enabled = false;
		rendererStatus.enabled = false;
		return rendererFront.enabled;
	}

	public bool enable(){
		rendererFront.enabled = true;
		rendererBack.enabled = true;
		rendererStatus.enabled = true;
		return rendererFront.enabled;
	}

//	public Path[,] getPaths() {
//		Paths = new PathEdge[portals.Count,portals.Count];
//		Path tpath;
//		for (int i = 0; i<portals.Count; i++) {
//			Paths [i, i] = null;
//			for (int j = i+1; j<portals.Count; j++) {
//				tpath = AStarZero.search (portals [i], portals [j]);
//				if (tpath != null) {
//					Paths [i, j] = tpath;
//					//tpath.Reverse ();
//					Paths [j, i] = tpath.GetReverse ();
//					Paths [i, j].Draw ();
//					Paths [j, i].Draw ();
//				} else {
//					Paths [i, j] = null;
//					Paths [j, i] = null;
//				}
//			}
//		}
//		return Paths;
//	}

	public List<Block> getPortals() {
//		Debug.LogFormat ("{0},{1} - {2},{3}", x, z, x + World.chunkSize, z + World.chunkSize);
		portals = new List<Block> ();
		int i;
		Block start, end, cur;
		if (z != 0) {
			for (i = 0; i<World.chunkSize; i++) {
				if (!(cur = World.blocks [x + i, y, z]).IsBlocking () && !World.blocks [x + i, y, z - 1].IsBlocking ()) {
					start = cur;
					while (i<World.chunkSize && !(cur = World.blocks[x + i, y, z]).IsBlocking () && !World.blocks[x + i++, y, z - 1].IsBlocking()) {
					}
					end = cur;
					if (start.X - end.X < 3) {
						portals.Add (World.blocks[(int)Mathf.Floor((start.X + end.X)/2f), y, z]);
//						res[QOrd.South][res[QOrd.South].Count-1].SetStatus(1);
					} else {
						portals.Add (start);
						portals.Add (end);
//						Debug.DrawLine(start.Position,Center(),Color.green,3f);
					}
				}
			}
		}
		if (z + World.chunkSize < World.sizex-1) {
			for (i = 0; i<World.chunkSize; i++) {
				if (!(cur = World.blocks [x + i, y, z + World.chunkSize - 1]).IsBlocking () && !World.blocks [x + i, y, z + World.chunkSize].IsBlocking ()) {
					start = cur;
					while (i<World.chunkSize && !(cur = World.blocks[x + i, y, z + World.chunkSize - 1]).IsBlocking () && !World.blocks[x + i++, y, z + World.chunkSize].IsBlocking()) {
					}
					end = cur;
					if (start.X - end.X < 3) {
						portals.Add (World.blocks[(int)Mathf.Floor((start.X + end.X)/2f), y, z + World.chunkSize - 1]);
						//						res[QOrd.South][res[QOrd.South].Count-1].SetStatus(1);
					} else {
						portals.Add (start);
						portals.Add (end);
					}
				}
			}
		}
		if (x != 0) {
			for (i = 0; i<World.chunkSize; i++) {
				if (!(cur = World.blocks [x, y, z + i]).IsBlocking () && !World.blocks [x - 1, y, z + i].IsBlocking ()) {
					start = cur;
					while (i<World.chunkSize && !(cur = World.blocks[x, y, z + i]).IsBlocking () && !World.blocks[x - 1, y, z + i++].IsBlocking()) {
					}
					end = cur;
					if (start.X - end.X < 3) {
						portals.Add (World.blocks[x, y, (int)Mathf.Floor((start.Z + end.Z)/2f)]);
						//						res[QOrd.South][res[QOrd.South].Count-1].SetStatus(1);
					} else {
						portals.Add (start);
						portals.Add (end);
					}
				}
			}
		}
		if (x < World.sizex - World.chunkSize) {
			for (i = 0; i<World.chunkSize; i++) {
				if (!(cur = World.blocks [x + World.chunkSize - 1, y, z + i]).IsBlocking () && !World.blocks [x + World.chunkSize, y, z + i].IsBlocking ()) {
					start = cur;
					while (i<World.chunkSize && !(cur = World.blocks[x + World.chunkSize - 1, y, z + i]).IsBlocking () && !World.blocks[x + World.chunkSize , y, z + i++].IsBlocking()) {
					}
					end = cur;
					if (start.X - end.X < 3) {
						portals.Add (World.blocks[start.X + World.chunkSize - 1, y, (int)Mathf.Floor((start.Z + end.Z)/2f)]);
						//						res[QOrd.South][res[QOrd.South].Count-1].SetStatus(1);
					} else {
						portals.Add (start);
						portals.Add (end);
					}
				}
			}
		}



		return portals;
	}

	public bool isEnabled(){
		return rendererFront.enabled;
	}

	public void updateChunk() {
		MeshData mdFront = new MeshData ();
		MeshData mdBack = new MeshData ();
		MeshData mdStatus = new MeshData ();
		if (updated) {
			//Debug.Log ("Updating"+x+","+y+","+z);
			updated = false;
			for (int i = 0; i < World.chunkSize; i++) {
				for (int k = 0; k < World.chunkSize; k++) {
					mdFront = World.blocks [x + i, y, z + k].MeshFront (mdFront);
					mdBack = World.blocks [x + i, y, z + k].MeshBack (mdBack);
					mdStatus = World.blocks [x + i, y, z + k].MeshStatus (mdStatus);
				}
			}
			RenderFront (mdFront);
			RenderBack (mdBack);
			RenderStatus (mdStatus);
		}
//		getPortals ();
//		getPaths ();
//		if(portals.Count > 0)
//			Debug.LogFormat ("{0},{1},{2} - {3}",x,y,z,portals.Count);
	}

	void RenderBack(MeshData md) {
		if (md.vertices.Count > 0) {
//			Debug.Log (md.vertices.Count + " " + md.triangles.Count + " " + md.uv.Count);
			backFilter.mesh.Clear ();
			backFilter.mesh.vertices = md.vertices.ToArray ();
			backFilter.mesh.SetTriangles(md.triangles.ToArray(),0);
			backFilter.mesh.uv = md.uv.ToArray ();
			backFilter.mesh.RecalculateNormals ();
		} else {
			rendererBack.enabled = false;
		}
	}

	void RenderFront(MeshData md) {
		if (md.vertices.Count > 0) {
			frontFilter.mesh.Clear ();
			frontFilter.mesh.vertices = md.vertices.ToArray ();
			frontFilter.mesh.SetTriangles(md.triangles.ToArray(),0);
			frontFilter.mesh.uv = md.uv.ToArray ();
			frontFilter.mesh.RecalculateNormals ();
			 
			coll.sharedMesh = null;
			Mesh mesh = new Mesh ();
			mesh.vertices = md.vertices.ToArray ();
			mesh.triangles = md.triangles.ToArray ();
			coll.sharedMesh = mesh;

		} else {
			rendererFront.enabled = false;
		}
	}

	void RenderStatus(MeshData md) {
		if (md.vertices.Count > 0) {
//			Debug.Log ("Rendering status" + md.vertices.Count);
			statusFilter.mesh.Clear ();
			statusFilter.mesh.vertices = md.vertices.ToArray ();
			statusFilter.mesh.SetTriangles(md.triangles.ToArray(),0);
			statusFilter.mesh.uv = md.uv.ToArray ();
			statusFilter.mesh.RecalculateNormals ();
		} else {
			rendererStatus.enabled = false;
		}
	}
}
