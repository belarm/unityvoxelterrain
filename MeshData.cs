﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshData {
	public List<Vector3> vertices = new List<Vector3>();
	public List<int> triangles = new List<int>();
//	public List<int> backTriangles = new List<int>();
//	public List<int> statusTriangles = new List<int>();
//	public List<int>[] subTris;
	public List<Vector2> uv = new List<Vector2>();

	public MeshData(){
//		subTris = new List<int>[3];
//		subTris [0] = new List<int> ();
//		subTris [1] = new List<int> ();
//		subTris [2] = new List<int> ();
	}

	public void AddQuadTriangles(){
		triangles.Add (vertices.Count - 4);
		triangles.Add (vertices.Count - 3);
		triangles.Add (vertices.Count - 2);
		
		triangles.Add (vertices.Count - 4);
		triangles.Add (vertices.Count - 2);
		triangles.Add (vertices.Count - 1);
	}

//	public void AddQuadTrianglesBack(){
//		backTriangles.Add (vertices.Count - 4);
//		backTriangles.Add (vertices.Count - 3);
//		backTriangles.Add (vertices.Count - 2);
//		
//		backTriangles.Add (vertices.Count - 4);
//		backTriangles.Add (vertices.Count - 2);
//		backTriangles.Add (vertices.Count - 1);
//	}
}
