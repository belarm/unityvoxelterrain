using UnityEngine;
using System.Collections;

public class BlockAir : Block {

	public BlockAir(int px, int py, int pz) : base(px, py, pz){
		Type = BType.AIR;
	}
	public BlockAir() : base(){
		Type = BType.AIR;
	}

	public override MeshData MeshFront (MeshData md)
	{
		return md;
	}

	public override MeshData MeshBack (MeshData md)
	{
		return md;
	}


	public override bool IsBlocking(){
		if (Y == 0)
			return false;
		if(!World.blocks[X,Y-1,Z].IsSolid(Direction.up))
		   return true;
		return false;
	}

	public override bool IsSolid (Direction dir)
	{
		return false;
	}
}
