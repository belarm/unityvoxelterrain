//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using UnityEngine.Assertions;
//
//
//
//public class OldQuadtree {
//
//
//	public OldQuadtreeNode Top;
//	public int Layer;
//	public int Count = 0;
//	public int DrawnCount = 0;
//	public int Size;
//	public static Func<OldQuadtreeNode,PathNode,bool>[] OrdTests = {OldQuadtree.findN,OldQuadtree.findS,OldQuadtree.findE,OldQuadtree.findW};
////	public bool beenprinted = false;
//
//	public OldQuadtree(int l, int s){
//		Size = s;
//		Layer = l;
//		Top = new OldQuadtreeNode (0, 0, QDir.NW, null, this, (int)Mathf.Log (Size,2));
//	}
//
//	public OldQuadtreeNode AddNode(uint addr, int depth=0) {
//		return Top.AddNode (addr,depth);
//	}
//
//	public static bool AreSameQuad(uint morton1, uint morton2, int depth) {
//		//Use bitshifts to compare two MBIs' left [depth] * 2 bits
//		morton1 >>= depth * 2;
//		morton2 >>= depth * 2;
//		return morton1 == morton2;	
//	}
//
//	public static uint CalcDir(uint addr,int depth) {
//		return addr >> (depth * 2) & 3;
//	}
//
//	public static bool CheckBounds(int x, int y) {
//		return ((x > 0) && (y > 0) && (x < World.sizex - 1) && (y < World.sizez - 1));
//	}
//	public void Clear() {
//		Top = new OldQuadtreeNode (0, 0, QDir.SW, null, this, (int)Mathf.Log (Size,2));
//	}
//
//	public OldQuadtreeNode SafeFind(int x, int y, int depth = 0) {
//		return CheckBounds(x,y) ? Top.FindNode((uint)x,(uint)y,depth,false) : null;
//	}
//
//	public static uint[] Decode( uint morton ) {
//		uint ex = morton;
//		uint ey = ( ex >> 1 );
//		ex &= 0x55555555;
//		ey &= 0x55555555;
//		ex |= ( ex >> 1 );
//		ey |= ( ey >> 1 );
//		ex &= 0x33333333;
//		ey &= 0x33333333;
//		ex |= ( ex >> 2 );
//		ey |= ( ey >> 2 );
//		ex &= 0x0f0f0f0f;
//		ey &= 0x0f0f0f0f;
//		ex |= ( ex >> 4 );
//		ey |= ( ey >> 4 );
//		ex &= 0x00ff00ff;
//		ey &= 0x00ff00ff;
//		ex |= ( ex >> 8 );
//		ey |= ( ey >> 8 );
//		ex &= 0x0000ffff;
//		ey &= 0x0000ffff;
//		return new uint[2] {ex,ey};
//	}
//
//	public static uint Encode(Vector2 v) {
//		return Encode((uint)v.x,(uint)v.y);
//	}
//
//	public static uint Encode( uint ex, uint ey ) {
//		ex &= 0x0000ffff;
//		ey &= 0x0000ffff;
//		ex |= ( ex << 8 );
//		ey |= ( ey << 8 );
//		ex &= 0x00ff00ff;
//		ey &= 0x00ff00ff;
//		ex |= ( ex << 4 );
//		ey |= ( ey << 4 );
//		ex &= 0x0f0f0f0f;
//		ey &= 0x0f0f0f0f;
//		ex |= ( ex << 2 );
//		ey |= ( ey << 2 );
//		ex &= 0x33333333;
//		ey &= 0x33333333;
//		ex |= ( ex << 1 );
//		ey |= ( ey << 1 );
//		ex &= 0x55555555;
//		ey &= 0x55555555;
//		return ex|(ey << 1);
//	}
//
//	public static bool findN (OldQuadtreeNode qtn,PathNode candidate) {
//		return candidate.Y == qtn.Y + OldQuadtree.QuadSize (qtn) - 1;
//	}
//
//	public static bool findS (OldQuadtreeNode qtn,PathNode candidate) {
//		return candidate.Y == qtn.Y;
//	}
//
//	public static bool findE (OldQuadtreeNode qtn,PathNode candidate) {
//		return candidate.X == qtn.X + OldQuadtree.QuadSize (qtn) - 1;
//	}
//
//	public static bool findW (OldQuadtreeNode qtn,PathNode candidate) {
//		return candidate.X == qtn.X;
//	}
//
//	public static int QuadSize(OldQuadtreeNode qtn) {
//		return 1 << qtn.QuadLayer;
//	}
//}
//
//public class OldQuadtreeNode : IComparable {
//
//	public uint Pos = 0;
//	private OldQuadtree Tree;
//	public OldQuadtreeNode Parent;
//	public OldQuadtreeNode[] Children;
//	public readonly int QuadLayer;
//	public int NumChildren = 0;
//	static public uint Horizontal = 0x55555555;
//	static public uint Vertical = 0xAAAAAAAA;
//	static public bool[] Adj = {false,true,true,false};
//	static public bool[,] Ords = {
//		{false,false,true,true},
//		{true,true,false,false},
//		{false,true,false,true},
//		{true,false,true,false}
//	};
//	static public uint[] Mirror = {0xAAAAAAAA,0xAAAAAAAA,0x55555555, 0x55555555};
//	public readonly int X,Y;
//	public List<PathNode> PortalNodes;
////	public HashSet<PathNode> PortalNodes;
////	public Dictionary<PathNode,List<PathEdge>> PortalEdges;
//
//
//	public OldQuadtreeNode(int px, int py, uint cp, OldQuadtreeNode p, OldQuadtree t, int d) {
//		X = px;
//		Y = py;
//		Parent = p;
//		Tree = t;
//		QuadLayer = d;
//		if (Parent != null)
//			Pos = (Parent.Pos << 2) + cp;
//		if (QuadLayer > 0) {
//			Children = new OldQuadtreeNode[4];
//			Children [QDir.NW] = null;
//			Children [QDir.NE] = null;
//			Children [QDir.SW] = null;
//			Children [QDir.SE] = null;
//		}
//		PortalNodes = new List<PathNode> ();
////		PortalEdges = new Dictionary<PathNode, List<PathEdge>> ();
////		tree.count++;
//	}
//	
//	public OldQuadtreeNode AddHole(uint addr, int depth = 0) {
//		uint cPos = OldQuadtree.CalcDir (addr, QuadLayer-1);
//		OldQuadtreeNode qt = GetChild (cPos);
//		if ((qt != null) && (qt.QuadLayer > 0) && (qt.isLeaf ())) {
//			qt.Fill ();
//			qt = GetChild (cPos);
//		}
//		if (QuadLayer > 1) {
//			if(qt == null)
//				qt = CreateChild (cPos);
//			return qt.AddHole (addr, depth);
//		} else {
//			if((qt = GetChild(cPos)) != null)
//				qt.Delete();
//			if(isLeaf ())
//				Prune ();
//			return this;
//		}
//	}
//
//	public OldQuadtreeNode AddNode(uint addr, int depth = 0) {
//		if (QuadLayer == depth) {
//			return this;
//		}
//		uint cpos = OldQuadtree.CalcDir (addr, QuadLayer-1);
//		OldQuadtreeNode cn = CreateChild (cpos), ret;
//		ret = cn.AddNode (addr,depth);
//		bool clear = true;
//		if (NumChildren == 4) {
//			for(int child = 0;child < 4; child++) {
//				clear &= Children[child].isLeaf();
//			}
//			if(clear){
//				NumChildren = 0;
//				Tree.Count -= 4;
//				Children = new OldQuadtreeNode[4];
//				return this;
//			}
//		}
//		return ret;
//	}
//
//	public bool CanGo(uint ordinal) {
//		switch(ordinal) {
//		case QOrd.North:
//			return Y + Size () < World.sizez;
//		case QOrd.South:
//			return Y > 0;
//		case QOrd.East:
//			return X > 0;
//		case QOrd.West:
//			return X + Size() < World.sizex;
//		}
//		return false;
//	}
//
//	public Vector3 Center() {
//		return new Vector3 (X + (float)Size () / 2 -.5f, Tree.Layer - .5f, Y + (float)Size () / 2 - .5f);
//	}
//	
//	public int CompareTo(object o) {
//		OldQuadtreeNode other = o as OldQuadtreeNode;
//
//		return Size ().CompareTo (other.Size ());
//	}
//
//	public int Size() {
//		return OldQuadtree.QuadSize(this);
//	}
//	
//	public OldQuadtreeNode CreateChild(uint p) {
//		if (!(Children [p] == null))
//			return Children [p];
//		NumChildren++;
//		int size = OldQuadtree.QuadSize(this);
//		switch (p) {
//		case (QDir.SW):
//			return Children [QDir.SW] = new OldQuadtreeNode (X, Y, QDir.SW, this, Tree, QuadLayer - 1);
//		case (QDir.SE):
//			return Children [QDir.SE] = new OldQuadtreeNode (X + size / 2, Y, QDir.SE, this, Tree, QuadLayer - 1);
//		case (QDir.NW):
//			return Children [QDir.NW] = new OldQuadtreeNode (X, Y + size / 2, QDir.NW, this, Tree, QuadLayer - 1);
//		case (QDir.NE):
//			return Children [QDir.NE] = new OldQuadtreeNode (X + size / 2, Y + size / 2, QDir.NE, this, Tree, QuadLayer - 1);
//		default:
//			return null;
//		}
//	}
//	
//	public void Delete() {
//		Parent.NumChildren--;
//		Parent.Children[Pos & 3] = null;
//		//parent.Prune ();
//	}
//
//	public void Fill() {
//		for(int i = 0;i<4;i++)
//			if(Children[i] == null)
//				CreateChild((uint)i);
//	}
//
//	public List<OldQuadtreeNode> FindNeighbors2(int depth = 0) {
//		OldQuadtreeNode qt;
//		List<OldQuadtreeNode> res = new List<OldQuadtreeNode> ();
//		qt = Tree.SafeFind (X - 1, Y, depth);
//		if (qt != null)
//			res.Add (qt);
//		qt = Tree.SafeFind (X, Y - 1, depth);
//		if (qt != null)
//			res.Add (qt);
//		qt = Tree.SafeFind (X + (1 << QuadLayer), Y, depth);
//		if (qt != null)
//			res.Add (qt);
//		qt = Tree.SafeFind (X, Y + (1 << QuadLayer), depth);
//		if (qt != null)
//			res.Add (qt);
//		return res;
//	}
//		
//
//
//
//	public OldQuadtreeNode FindNode(uint tx, uint ty, int depth = 0, bool draw = false) {
//		return FindNode (OldQuadtree.Encode (tx, ty),depth,draw);
//	}
//
//	public OldQuadtreeNode FindNode(uint addr, int depth = 0, bool draw = false) {
//		if ((addr == Pos) || (QuadLayer == 0) || (QuadLayer == depth) || isLeaf ())
//			return this;
//		uint cpos = OldQuadtree.CalcDir (addr, QuadLayer - 1);
//		if (Children [cpos] == null)
//			return null;
//		if(draw)
//			DrawPath (Children [cpos],Color.white);
//		return Children [cpos].FindNode (addr,depth,draw);
//	}
//
//	public OldQuadtreeNode GetChild(uint child) {
//		return Children [(uint)child];
//	}
//
//	public List<OldQuadtreeNode> GetSide(int ordinal) {
//		List<OldQuadtreeNode> ret = new List<OldQuadtreeNode> ();
//		for (int i = 0; i<3; i++) {
//			if(Ords[ordinal,i] && (Children[i] != null)) {
//				if(Children[i].isLeaf()) {
//					ret.Add (Children[i]);
//				} else {
//					ret.AddRange(Children[i].GetSide(ordinal));
//				}
//			}
//		}
//		return ret;
//	}
//
//	public uint GetMorton() {
//		return Pos << QuadLayer * 2;
//	}
//
//	public int H(OldQuadtreeNode dest) {
//		return (int)Vector3.Distance (Center (), dest.Center ());
//	}
//
//
//
//	public bool isFull() {
//		return (NumChildren == 4);
//	}
//
//	public bool isLeaf() {
//		return (NumChildren == 0);
//	}
//	
//	public List<PathNode> OuterPortals() {
//		List<PathNode> ret = new List<PathNode> ();
//		uint s1=5, s2=5;
//		switch (Pos & 3) {
//		case QDir.NE:
//			s1 = QOrd.North;
//			s2 = QOrd.East;
//			break;
//		case QDir.NW:
//			s1 = QOrd.North;
//			s2 = QOrd.West;
//			break;
//		case QDir.SE:
//			s1 = QOrd.South;
//			s2 = QOrd.East;
//			break;
//		case QDir.SW:
//			s1 = QOrd.South;
//			s2 = QOrd.West;
//			break;
//		default:
//			break;
//		}
//		foreach (PathNode next in PortalNodes) {
//			if ((OldQuadtree.OrdTests [s1] (this, next)) || (OldQuadtree.OrdTests [s2] (this, next))) {
//				ret.Add (next);
//			}
//		}
//		return ret;
//	}
//
//	public void Paths() {
//		//PortalEdges = new Dictionary<PathNode, List<PathEdge>> ();
//		PathEdge temp;
//		if (isLeaf ()) {
//			PortalNodes = Portals ();
//			foreach (PathNode startnode in PortalNodes) {
//				if(!Path.EdgeCache.ContainsKey(startnode)) { Path.EdgeCache[startnode] = new List<PathEdge>(); }
//				foreach(PathNode endnode in PortalNodes) {
//					if(startnode != endnode) {
//						temp = new PathEdge(startnode,endnode);
//						Path.EdgeCache[startnode].Add (temp);
//					}
//				}
//			}
//		} else {
//			PortalNodes = new List<PathNode>();
////			PortalNodes = Portals();
//			for(int i = 0;i<4;i++) {
//				if (Children [i] != null) {
//					Children [i].Paths ();
//					PortalNodes.AddRange(Children[i].OuterPortals());
//				}
//			}
//			Path tempPath;
//			foreach (PathNode startnode in PortalNodes) {
//				if(!Path.EdgeCache.ContainsKey(startnode)) { Path.EdgeCache[startnode] = new List<PathEdge>(); }
//				foreach(PathNode endnode in PortalNodes) {
//					if(startnode != endnode) {
//						Debug.DrawLine(startnode.Position,endnode.Position,Color.yellow,5f);
//						if(!Path.EdgeCache[startnode].Contains(new PathEdge(startnode,endnode))) {
//							tempPath = AStarZero.search(startnode,endnode,false);
//							if(tempPath != null) {
//								Path.EdgeCache[startnode].Add (tempPath);
//								Path.EdgeCache[endnode].Add (tempPath.GetReverse());
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//
//	public List<PathNode> Portals() {
////		List<PathNode> PortalNodes = new List<PathNode> ();
//		int size = OldQuadtree.QuadSize (this);
//		OldQuadtreeNode reflection;
//		PathNode tempNode, exitNode;
//		PathEdge interEdge;
//		if (X > 0) {
//			for (int cy = 0; cy < size; cy++) {
//				if(!World.blocks[X,Tree.Layer,Y+cy].IsBlocking() && !World.blocks[X-1,Tree.Layer,Y+cy].IsBlocking()) {
//					tempNode = new PathNode(X,Tree.Layer,Y+cy);
//					exitNode = new PathNode(X-1,Tree.Layer,Y+cy);
//					reflection = Tree.Top.FindNode(Morton.Encode (X-1,Y+cy));
//					if(!PortalNodes.Contains(tempNode)) { PortalNodes.Add (tempNode);}
//					if(!reflection.PortalNodes.Contains(exitNode)) { reflection.PortalNodes.Add (exitNode);}
//					if(!Path.EdgeCache.ContainsKey(tempNode))
//						Path.EdgeCache[tempNode] = new List<PathEdge>();
//					if(!Path.EdgeCache.ContainsKey(exitNode))
//						Path.EdgeCache[exitNode] = new List<PathEdge>();
//					interEdge = new PathEdge(tempNode,exitNode,true,1);
//					Path.EdgeCache[tempNode].Add(interEdge);
//					Path.EdgeCache[exitNode].Add(interEdge.GetReverse());
//					while((++cy < size) && !World.blocks[X,Tree.Layer,Y+(cy)].IsBlocking() && !World.blocks[X-1,Tree.Layer,Y+cy].IsBlocking()) {
//					}
//					cy--;
//					tempNode = new PathNode(X,Tree.Layer,Y+cy);
//					exitNode = new PathNode(X-1,Tree.Layer,Y+cy);
//					reflection = Tree.Top.FindNode(Morton.Encode(X-1,Y+cy));
//					if(!PortalNodes.Contains(tempNode)) { PortalNodes.Add (tempNode);}
//					if(!reflection.PortalNodes.Contains(exitNode)) { reflection.PortalNodes.Add (exitNode);}
//					if(!Path.EdgeCache.ContainsKey(tempNode))
//						Path.EdgeCache[tempNode] = new List<PathEdge>();
//					if(!Path.EdgeCache.ContainsKey(exitNode))
//						Path.EdgeCache[exitNode] = new List<PathEdge>();
//					interEdge = new PathEdge(tempNode,exitNode,true,1);
//					Path.EdgeCache[tempNode].Add(interEdge);
//					Path.EdgeCache[exitNode].Add(interEdge.GetReverse());
//				}
//			}
//		}
////		if (X + size < World.sizex) {
////			int maxx = X + size - 1;
////			for (int cy = 0; cy < size; cy++) {
////				if(!World.blocks[maxx,Tree.Layer,Y+cy].IsBlocking() && !World.blocks[maxx + 1,Tree.Layer,Y+cy].IsBlocking()) {
////					tempNode = new PathNode(maxx,Tree.Layer,Y+cy);
////					if(!portals.Contains(tempNode)) { portals.Add (tempNode);}
////					while((++cy < size) && !World.blocks[maxx,Tree.Layer,Y+(cy)].IsBlocking() && !World.blocks[maxx+1,Tree.Layer,Y+cy].IsBlocking()) {
////					}
////					cy--;
////					tempNode = new PathNode(maxx,Tree.Layer,Y+cy);
////					if(!portals.Contains(tempNode)) { portals.Add (tempNode);}
////				}
////			}
////		}
//
//		if (Y > 0) {
//			for (int cx = 0; cx < size; cx++) {
//				if(!World.blocks[X+cx,Tree.Layer,Y].IsBlocking() && !World.blocks[X+cx,Tree.Layer,Y-1].IsBlocking()) {
//					tempNode = new PathNode(X+cx,Tree.Layer,Y);
//					exitNode = new PathNode(X+cx,Tree.Layer,Y-1);
//					reflection = Tree.Top.FindNode(Morton.Encode (X+cx,Y-1));
//					if(!PortalNodes.Contains(tempNode)) { PortalNodes.Add (tempNode);}
//					if(!reflection.PortalNodes.Contains(exitNode)) { reflection.PortalNodes.Add (exitNode);}
//					if(!Path.EdgeCache.ContainsKey(tempNode))
//						Path.EdgeCache[tempNode] = new List<PathEdge>();
//					if(!Path.EdgeCache.ContainsKey(exitNode))
//						Path.EdgeCache[exitNode] = new List<PathEdge>();
//					interEdge = new PathEdge(tempNode,exitNode,true,1);
//					Path.EdgeCache[tempNode].Add(interEdge);
//					Path.EdgeCache[exitNode].Add(interEdge.GetReverse());
//					while((++cx < size) && !World.blocks[X+cx,Tree.Layer,Y].IsBlocking() && !World.blocks[X+cx,Tree.Layer,Y-1].IsBlocking()) {
//					}
//					cx--;
//					tempNode = new PathNode(X+cx,Tree.Layer,Y);
//					exitNode = new PathNode(X+cx,Tree.Layer,Y-1);
//					reflection = Tree.Top.FindNode(Morton.Encode (X+cx,Y-1));
//					if(!PortalNodes.Contains(tempNode)) { PortalNodes.Add (tempNode);}
//					if(!reflection.PortalNodes.Contains(exitNode)) { reflection.PortalNodes.Add (exitNode);}
//					if(!Path.EdgeCache.ContainsKey(tempNode))
//						Path.EdgeCache[tempNode] = new List<PathEdge>();
//					if(!Path.EdgeCache.ContainsKey(exitNode))
//						Path.EdgeCache[exitNode] = new List<PathEdge>();
//					interEdge = new PathEdge(tempNode,exitNode,true,1);
//					Path.EdgeCache[tempNode].Add(interEdge);
//					Path.EdgeCache[exitNode].Add(interEdge.GetReverse());
//				}
//			}
//		}
////		if (Y + size < World.sizez) {
////			int maxy = Y + size - 1;
////			for (int cx = 0; cx < size; cx++) {
////				if(!World.blocks[X+cx,Tree.Layer,maxy].IsBlocking() && !World.blocks[X+cx,Tree.Layer,maxy + 1].IsBlocking()) {
////					tempNode = new PathNode(X + cx,Tree.Layer,maxy);
////					if(!portals.Contains(tempNode)) { portals.Add (tempNode);}
////					while((++cx < size) && !World.blocks[X+cx,Tree.Layer,maxy].IsBlocking() && !World.blocks[X+cx,Tree.Layer,maxy+1].IsBlocking()) {
////					}
////					cx--;
////					tempNode = new PathNode(X+cx,Tree.Layer,maxy);
////					if(!portals.Contains(tempNode)) { portals.Add (tempNode);}
////				}
////			}
////		}
//		return PortalNodes;
//	}
//
//	public List<PathNode> PortalsBySide (uint ordinal) {
//		List<PathNode> ret = new List<PathNode> ();
//		foreach (PathNode next in PortalNodes) {
//			if(OldQuadtree.OrdTests[ordinal](this,next))
//				ret.Add (next);
//		}
//		return ret;
//	} 
//
//	public void Prune() {
//		if (isLeaf ())
//			Delete ();
//		if (QuadLayer != World.maxDepth)
//			Parent.Prune ();
//	}
//
//	//Debug Functions
//
//	public void DrawIfEmpty() {
//		if (isLeaf ())
//			DrawQuad ();
//		else {
//			if (QuadLayer > 0) {
//				if (Children [QDir.NW] != null)
//					Children [QDir.NW].DrawIfEmpty ();
//				if (Children [QDir.NE] != null)
//					Children [QDir.NE].DrawIfEmpty ();
//				if (Children [QDir.SW] != null)
//					Children [QDir.SW].DrawIfEmpty ();
//				if (Children [QDir.SE] != null)
//					Children [QDir.SE].DrawIfEmpty ();
//			}
////			foreach (PathNode next in PortalNodes) {
////				Debug.DrawLine (Center (), next.Position + Vector3.down/2, Color.yellow, 5f);
////			}
//		}
//	}
//	public void DrawPaths() {
//		if (isLeaf ()) {
//			foreach (PathNode node in PortalNodes) {
//				foreach (PathEdge edge in Path.EdgeCache[node]) {
//					Debug.DrawLine (edge.Start.Position + Vector3.down/2f, edge.End.Position + Vector3.down/2f, Color.white, 5f);
//				}
//			}
//		} else {
//			for (int i = 0; i<4; i++)
//				if (Children [i] != null)
//					Children [i].DrawPaths ();
//		}
//	}
//
//	public void DrawQuad() {
//		Tree.DrawnCount++;
//		Vector3[] corners = new Vector3[4];
//		int w, h;
//		w = h = (int)Math.Pow (2,QuadLayer);
//		Color shade = isLeaf() ? Color.red : Color.white;
//		corners [QDir.NW] = new Vector3 (X - 0.5f, Tree.Layer-.5f, Y - 0.5f);
//		corners [QDir.NE] = new Vector3 (X - 0.5f, Tree.Layer-.5f, Y + h - 0.5f);
//		corners [QDir.SW] = new Vector3 (X + w - 0.5f, Tree.Layer-.5f, Y + h - 0.5f);
//		corners [QDir.SE] = new Vector3 (X + w - 0.5f, Tree.Layer-.5f, Y - 0.5f);
//		Debug.DrawLine (corners [0], corners [1], shade, 5f-QuadLayer/2);
//		Debug.DrawLine (corners [1], corners [2], shade, 5f-QuadLayer/2);
//		Debug.DrawLine (corners [2], corners [3], shade, 5f-QuadLayer/2);
//		Debug.DrawLine (corners [3], corners [0], shade, 5f-QuadLayer/2);
////		foreach (PathNode next in PortalNodes) {
////			Debug.DrawLine (Center (), next.Position + Vector3.down/2, Color.yellow, 5f);
////		}
//	}
//
//	public void DrawQuad2(Color shade) {
//		//tree.dcount++;
//		Vector3[] corners = new Vector3[4];
//		int w, h;
//		w = h = (int)Math.Pow (2,QuadLayer);
//		corners [QDir.NW] = new Vector3 (X - 0.5f, Tree.Layer-.5f, Y - 0.5f);
//		corners [QDir.NE] = new Vector3 (X - 0.5f, Tree.Layer-.5f, Y + h - 0.5f);
//		corners [QDir.SW] = new Vector3 (X + w - 0.5f, Tree.Layer-.5f, Y + h - 0.5f);
//		corners [QDir.SE] = new Vector3 (X + w - 0.5f, Tree.Layer-.5f, Y - 0.5f);
//		Debug.DrawLine (corners [0], corners [1], shade, 10f-QuadLayer/2);
//		Debug.DrawLine (corners [1], corners [2], shade, 10f-QuadLayer/2);
//		Debug.DrawLine (corners [2], corners [3], shade, 10f-QuadLayer/2);
//		Debug.DrawLine (corners [3], corners [0], shade, 10f-QuadLayer/2);
////		Debug.DrawLine (corners [0], corners [2], shade, 5f-qlayer/2);
////		Debug.DrawLine (corners [1], corners [3], shade, 5f-qlayer/2);
//	}
//
//	public void DrawPath(OldQuadtreeNode dest, Color colr) {
//		Debug.DrawLine (Center () + (Vector3.up * QuadLayer ),dest.Center () + (Vector3.up * dest.QuadLayer), colr,100f);
//	}
//}