using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;


public class World : MonoBehaviour
{
	public static World gWorld;
	public static Block[,,] blocks;
	public static Chunk[,,] chunks;
	public static int chunkSize = 16;
	public static int maxDepth;
	public float noiseScale = 6.5f;
	public float noiseOffset = 0f;
	public int dimx, dimy, dimz;
	public static int sizex, sizey, sizez;
	public Material[] terrainMat;
	public Material lineMat;
	public CameraControl camControl;
	public GameObject SelectBox;
	public Queue<Vector2> blockParser;
	public static Texture selectTexture;
	public static int MaxMorton;
	private Vector2 last;
	private int[] pow4 = { 1, 4, 16, 64, 256, 1024, 4096, 16384, 65536, 262144, 1048576 };
	private SelectionSystem selecter;
	public List<Block> selectedBlocks;
	public static int startingLayer;
	public static Quadtree[] qTree;
	public static StreamWriter DOTFile;

	void Start ()
	{
		gWorld = this;
		sizex = dimx;
		sizey = dimy;
		sizez = dimz;
		MaxMorton = Morton.Encode (sizex - 1, sizez - 1);
		maxDepth = (int)Mathf.Ceil (Mathf.Log (sizex, 2));
		int i, j, k;
		startingLayer = camControl.layer;
		selecter = gameObject.AddComponent<SelectionSystem> ();
		selecter.selectTexture = selectTexture;
		selecter.enabled = true;
	
		blocks = new Block[sizex, sizey, sizez];
		chunks = new Chunk[sizex / chunkSize, sizey, sizez / chunkSize];
		qTree = new Quadtree[sizey];
		blockParser = new Queue<Vector2> ();
		DOTFile = new StreamWriter ("Quadtree.dot");

		//Generate perlin noise heightmap. Temp terrain gen.
		int[,] heightmap = new int[sizex, sizez];

		for (i = 0; i < sizex; i++) {
			for (k = 0; k < sizez; k++) {
				heightmap [i, k] = Mathf.RoundToInt (Mathf.PerlinNoise ((i + noiseOffset) * noiseScale, (k + noiseOffset) * noiseScale) * (sizey / 5) + sizey / 2);
			}
		}
		//-----
		float runtime = Time.realtimeSinceStartup;

		for (i = 0; i < sizex; i++) {
			for (j = 0; j < sizey; j++) {
				for (k = 0; k < sizez; k++) {
					if (i % chunkSize == 0 && k % chunkSize == 0) {
						chunks [i / chunkSize, j, k / chunkSize] = new Chunk (this, i, j, k);
					}
					if (j < heightmap [i, k]) {
						blocks [i, j, k] = new Block (i, j, k);
					} else {
						blocks [i, j, k] = new BlockAir (i, j, k);
					}
				}
			}
		}
		Debug.LogFormat ("Blocks set in {0}s", Time.realtimeSinceStartup - runtime);
		runtime = Time.realtimeSinceStartup;

		for (j = 0; j < sizey; j++) {
			qTree [j] = new Quadtree (j);
			LoadQuad (0, j, 0, maxDepth);
			blockParser.Enqueue (new Vector2 (-1f, -1f));
			ParseQueue (qTree [j], blockParser);
			qTree [j].WriteDOT ();
		}

		Debug.LogFormat ("Quadtree generated in {0}s", Time.realtimeSinceStartup - runtime);
		runtime = Time.realtimeSinceStartup;
		for (j = 0; j < sizey; j++)
			qTree [j].Top.LinkNeighbors ();
		Debug.LogFormat ("Neighbors cached in {0}s", Time.realtimeSinceStartup - runtime);
		//qTree [18].top.DrawIfEmpty ();

		//qTree [18].top.AddNode (0U, 5);



//		GetQuad (0, 18, 0,(int)Mathf.Log(sizez,2));
//		blockParser.Enqueue (new Vector2 (-1f, -1f));
//		ParseQueue (qTree [18], blockParser);
//
//		Debug.Log (blockParser.Count);
//		Debug.Log (qTree [18].count);
//		qTree [18].top.DrawIfEmpty ();

		//qTree [18].drawQuad ();
		runtime = Time.realtimeSinceStartup;
		for (i = 0; i < sizex / chunkSize; i++) {
			for (j = 0; j < sizey; j++) {
				for (k = 0; k < sizez / chunkSize; k++) {
					chunks [i, j, k].updated = true;
					chunks [i, j, k].updateChunk ();
					if (j <= camControl.layer && j >= camControl.layer - camControl.renderDepth) {
						chunks [i, j, k].enable ();
					}
//					pathcount += chunks [i, j, k].portals.Count;
				}
//				qTree[j].Top.Paths();
			}
		}
//		Debug.LogFormat ("{1} Chunk portals found in {0:0.000}s", Time.realtimeSinceStartup - runtime, PathDB.Nodes [18].Count);
//		AStarSearch.search (qTree [17], 30, 20, 40, 20);
//		Debug.DrawLine (Vector3.zero, Vector3.up * sizey, Color.blue, 100f);
//		Debug.DrawLine (Vector3.right, Vector3.right + Vector3.up * sizey, Color.blue, 100f);
		//PathDB.PrintEdges (18);

		PathEdge temp = new PathEdge (new PathNode (10, 10, 10), new PathNode (10, 10, 11));
		PathEdge temp2 = new PathEdge (new PathNode (10, 10, 10), new PathNode (10, 10, 11));
		PathNode n1 = new PathNode (10, 10, 10);
		PathNode n2 = new PathNode (10, 10, 10);
		List<PathEdge> edgelist = new List<PathEdge> ();
		List<PathNode> nodelist = new List<PathNode> ();
		edgelist.Add (temp);
		nodelist.Add (n1);
		Debug.Log (edgelist.Contains (temp2) + " " + nodelist.Contains (n2) + " " + (n2 == n1));
//		Debug.Log (PathNode.Equals (n1, n2));
		Debug.Log (n1 == n2);
		DOTFile.Flush ();
		Debug.Log ("-----------------------");
	}

	public void ParseQueue (Quadtree qt, Queue<Vector2> toParse)
	{
		List<Vector2> tempQ = new List<Vector2> ();
		Vector2 hold;
		while (toParse.Count > 0) {
			hold = toParse.Dequeue ();
			if (hold.x != -1f) {
				tempQ.Add (hold);
			} else {
				for (int p = maxDepth; p >= 0; p--) {
					if (tempQ.Count < pow4 [p] || (tempQ [0].x % pow4 [p] != 0) || (tempQ [0].y % pow4 [p] != 0))
						continue;
					if (Morton.AreSameQuad (Morton.Encode (tempQ [0]), Morton.Encode (tempQ [pow4 [p] - 1]), p)) {
						qt.AddNode (Morton.Encode (tempQ [0]), p);
						tempQ.RemoveRange (0, pow4 [p]);
					} else
						continue;
					p = maxDepth;
				}
			}
		}
	}

	public void LoadQuad (int x, int y, int z, int pow)
	{
		if (pow <= 1) {
			ZLoad (x, y, z);
		} else {
			pow--;
			int offset = (int)System.Math.Pow (2, pow);
			LoadQuad (x, y, z, pow);
			LoadQuad (x + offset, y, z, pow);
			LoadQuad (x, y, z + offset, pow);
			LoadQuad (x + offset, y, z + offset, pow);
		}
	}

	public QTNode FindQuad (int x, int y, int z)
	{
		return qTree [y].Top.FindNodeXY (x, z);
	}

	public QTNode FindQuad (Vector3 pos, int depth = 0, bool draw = false)
	{
		return qTree [(int)pos.y].Top.FindNodeXY (Morton.Encode ((int)pos.x, (int)pos.z), depth);
	}

	public QTNode FindQuad (int addr, int y)
	{
		return qTree [y].Top.FindNode (addr);
	}

	public void FindPath2 (Block p1, Block p2, int method = 0)
	{
		if (p1.GetParent () == p2.GetParent ())
			AStarZero.search (new PathNode (p1.Position), new PathNode (p2.Position));
		else
			Debug.Log ("Different Chunks");
	}

	public void FindPath (Vector3 p1, Vector3 p2, int method = 0)
	{
		if (p1.y == p2.y) {
			if (method == 0)
				AStarSearch.search (qTree [(int)p1.y], (int)p1.x, (int)p1.z, (int)p2.x, (int)p2.z);
			else if (method == 1)
				AStarSearch.search2 (qTree [(int)p1.y], (int)p1.x, (int)p1.z, (int)p2.x, (int)p2.z);
//			else 
//				AStarZero.search (p1,p2);
		}

	}


	public void ZLoad (int x, int y, int z)
	{
		if (blocks [x, y, z].IsBlocking ()) {
			if (last.x != -1) {
				blockParser.Enqueue (last = new Vector2 (-1f, -1f));
			}
		} else {
			blockParser.Enqueue (last = new Vector2 (x, z));
		}
		if (blocks [x + 1, y, z].IsBlocking ()) {
			if (last.x != -1) {
				blockParser.Enqueue (last = new Vector2 (-1f, -1f));
			}
		} else {
			blockParser.Enqueue (last = new Vector2 (x + 1, z));
		}
		if (blocks [x, y, z + 1].IsBlocking ()) {
			if (last.x != -1) {
				blockParser.Enqueue (last = new Vector2 (-1f, -1f));
			}
		} else {
			blockParser.Enqueue (last = new Vector2 (x, z + 1));
		}
		if (blocks [x + 1, y, z + 1].IsBlocking ()) {
			if (last.x != -1) {
				blockParser.Enqueue (last = new Vector2 (-1f, -1f));
			}
		} else {
			blockParser.Enqueue (last = new Vector2 (x + 1, z + 1));
		}
	}

	void Update ()
	{
		for (int i = 0; i < sizex / chunkSize; i++) {
			for (int j = 0; j < camControl.renderDepth + 1 && camControl.layer - j >= 0; j++) {
				for (int k = 0; k < sizez / chunkSize; k++) {
					if (chunks [i, camControl.layer - j, k].updated) {
						chunks [i, camControl.layer - j, k].updateChunk ();
					}
				}
			}
		}
	}

	public void deleteBlock (Block b)
	{
		deleteBlock (b.GetPos ());
	}

	public void deleteBlock (int[] pos)
	{
		deleteBlock (pos [0], pos [1], pos [2]);
	}

	public void deleteBlock (int x, int y, int z)
	{
		if (blocks [x, y, z].Type == BType.NOTAIR) {
			//blocks[x,y,z] = new BlockAir();
			setBlock<BlockAir> (x, y, z);
			qTree [y].Top.AddNode (Morton.Encode (x, z));
//		QuadtreeNode qt = qTree [y + 1].top.FindNode (Quadtree.Encode (x, z));
//		qt.Empty ();
//		blockParser.Clear ();
//		GetQuad(qt.x,y+1,qt.y,qt.qlayer);
//		blockParser.Enqueue(new Vector2(-1f,-1f));
//		ParseQueue(qTree[y+1],blockParser);
			qTree [y + 1].Top.AddHole (Morton.Encode (x, z));
		}
	}

	public static Block getBlock (Vector3 pos)
	{
		if (pos.x < sizex && pos.y < sizey && pos.z < sizez && pos.x >= 0 && pos.y >= 0 && pos.z >= 0)
			return blocks [(int)pos.x, (int)pos.y, (int)pos.z];
		else
			return null;
	}

	public static Block getBlock (int x, int y, int z)
	{
		if (x < sizex && y < sizey && z < sizez && x >= 0 && y >= 0 && z >= 0)
			return blocks [x, y, z];
		else
			return null;
	}

	public static Block setBlock<T> (int x, int y, int z) where T : new()
	{
		//block.setPos (x, y, z);
		blocks [x, y, z] = new T () as Block;
		blocks [x, y, z].GetParent ().updated = true;
		blocks [x + 1, y, z].GetParent ().updated = true;
		blocks [x - 1, y, z].GetParent ().updated = true;
		blocks [x, y + 1, z].GetParent ().updated = true;
		blocks [x, y - 1, z].GetParent ().updated = true;
		blocks [x, y, z + 1].GetParent ().updated = true;
		blocks [x, y, z - 1].GetParent ().updated = true;
		return blocks [x, y, z];
	}

	public static Block setBlock<T> (int[] coords) where T : new()
	{
		return setBlock<T> (coords [0], coords [1], coords [2]);
	}

	public bool SetLayer (int nlayer)
	{
		selecter.SetLayer (nlayer);
		return true;
	}

	public void DrawQuads (int layer, int depth = 0)
	{
		//qTree [layer].Top.CachePaths ();
		qTree [layer].Top.DrawIfEmpty (depth);
//		qTree [layer].Top.DrawPaths ();
		PathDB.PrintEdges (layer);
//		Debug.Log ("PathDB contains " + PathDB.NodeCount + " nodes and " + PathDB.PathCount + " paths");
		int pathcount = 0;
//		for (int j = 0; j < World.maxDepth; j++)
		foreach (var edgelist in PathDB.Edges[layer]) {
			pathcount += edgelist.Value.Count;
			foreach (PathEdge edge in edgelist.Value)
				DOTFile.WriteLine ("PN_{0}_{1} -> PN_{2}_{3} [label={4}] ", edge.Start.X, edge.Start.Z, edge.End.X, edge.End.Z,edge.Level);
		}
		DOTFile.Flush ();
		Debug.LogFormat ("Level {0} contains {1} nodes and {2} paths", layer, PathDB.Edges [layer].Count, pathcount);

	}
}
