﻿using UnityEngine;
using System.Collections;

public class SelectionSystem : MonoBehaviour {
	private Vector3 pos0 = Vector3.zero, pos1 = Vector3.zero;
	public Texture selectTexture;
	private GameObject sBox;
	private GameObject colliderPlane;
	private MeshRenderer mrender;
	private MeshFilter filter;
	private MeshCollider selectionColl;
	private int y;
	private GameObject box;
	private GameObject anchor;
	private World gWorld;

	void Start() {
		selectTexture = World.selectTexture;
		sBox = new GameObject ();
		colliderPlane = new GameObject ();
		filter = sBox.AddComponent<MeshFilter> ();
		mrender = sBox.AddComponent<MeshRenderer> ();
		y = World.startingLayer;

		selectionColl = colliderPlane.AddComponent<MeshCollider> ();
		selectionColl.name = "Selction helper plane";
		selectionColl.gameObject.layer = Physics.IgnoreRaycastLayer;
		MeshData md = new MeshData ();
		BuildBox (md);
		filter.mesh.vertices = md.vertices.ToArray();
		filter.mesh.SetTriangles(md.triangles.ToArray(),0);
		mrender.material = World.gWorld.lineMat;
		mrender.enabled = false;

		Vector3[] verts = new Vector3[4];
		verts [0] = new Vector3 (0, 0, 0);
		verts [1] = new Vector3 (World.sizex, 0, 0);
		verts [2] = new Vector3 (World.sizex, 0, World.sizez);
		verts [3] = new Vector3 (0, 0, World.sizez);
		Mesh tmesh = new Mesh();
		tmesh.vertices = verts;
		tmesh.SetTriangles(new int[] {2,1,0,3,2,0,0,1,2,0,2,3},0);
		selectionColl.sharedMesh = tmesh;
		selectionColl.transform.localScale = 5 * Vector3.one;
		selectionColl.transform.position = new Vector3 (-2 * World.sizex, y, -2 * World.sizez);


	}

	void Update() {
		RaycastHit hit;
		Vector3 oldpos1;
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			if (selectionColl.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 1000)) {
				//oldpos0 = pos0;
				pos0 = sBox.transform.position = TerrainHit.GetBlockPosV (hit.point);
			}
		} else if (Input.GetKey (KeyCode.Mouse0)) {
			if (selectionColl.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 1000)) {
				oldpos1 = pos1;
				pos1 = TerrainHit.GetBlockPosV (TerrainHit.Constrain (hit.point));
				if (oldpos1 != pos1)
					Outline (pos0, pos1);
			}
			//Outline (pos0,pos1);
		} else if (Input.GetKeyUp (KeyCode.Mouse0)) {
			Vector3 p0 = new Vector3 (Mathf.Min (pos0.x, pos1.x, World.sizex-1), Mathf.Min (pos0.y, pos1.y, World.sizey-1), Mathf.Min (pos0.z, pos1.z, World.sizez-1));
			Vector3 p1 = new Vector3 (Mathf.Max (pos0.x, pos1.x, 0), Mathf.Max (pos0.y, pos1.y, 0), Mathf.Max (pos0.z, pos1.z, 0));
			for (int i = (int)p0.x; i <= (int)p1.x; i++)
				for (int j = (int)p0.y; j <= (int)p1.y; j++)
					for (int k = (int)p0.z; k <= (int)p1.z; k++)
						if (!World.blocks [i, j, k].IsAir())
							World.blocks [i, j, k].SetStatus (0);
			pos1 = pos0 = Vector3.zero;
			mrender.enabled = false;
		} else {
			pos1 = pos0 = Vector3.zero;
			mrender.enabled = false;
		}
	}
	private void BuildBox(MeshData md) {
		md.vertices.Add (new Vector3 (0, 1, 1));
		md.vertices.Add (new Vector3 (1, 1, 1));
		md.vertices.Add (new Vector3 (1, 1, 0));
		md.vertices.Add (new Vector3 (0, 1, 0));
		md.AddQuadTriangles ();
		md.vertices.Add (new Vector3 (1, 1, 1));
		md.vertices.Add (new Vector3 (0, 1, 1));
		md.vertices.Add (new Vector3 (0, 1, 0));
		md.vertices.Add (new Vector3 (1, 1, 0));
		md.AddQuadTriangles ();
		
		md.vertices.Add (new Vector3 (0, 0, 0));
		md.vertices.Add (new Vector3 (1, 0, 0));
		md.vertices.Add (new Vector3 (1, 0, 1));
		md.vertices.Add (new Vector3 (0, 0, 1));
		md.AddQuadTriangles ();
		md.vertices.Add (new Vector3 (1, 0, 0));
		md.vertices.Add (new Vector3 (0, 0, 0));
		md.vertices.Add (new Vector3 (0, 0, 1));
		md.vertices.Add (new Vector3 (1, 0, 1));
		md.AddQuadTriangles ();
		
		md.vertices.Add (new Vector3 (1, 0, 1));
		md.vertices.Add (new Vector3 (1, 1, 1));
		md.vertices.Add (new Vector3 (0, 1, 1));
		md.vertices.Add (new Vector3 (0, 0, 1));
		md.AddQuadTriangles ();
		md.vertices.Add (new Vector3 (1, 1, 1));
		md.vertices.Add (new Vector3 (1, 0, 1));
		md.vertices.Add (new Vector3 (0, 0, 1));
		md.vertices.Add (new Vector3 (0, 1, 1));
		md.AddQuadTriangles ();
		
		md.vertices.Add (new Vector3 (1, 0, 0));
		md.vertices.Add (new Vector3 (1, 1, 0));
		md.vertices.Add (new Vector3 (1, 1, 1));
		md.vertices.Add (new Vector3 (1, 0, 1));
		md.AddQuadTriangles ();
		md.vertices.Add (new Vector3 (1, 1, 0));
		md.vertices.Add (new Vector3 (1, 0, 0));
		md.vertices.Add (new Vector3 (1, 0, 1));
		md.vertices.Add (new Vector3 (1, 1, 1));
		md.AddQuadTriangles ();
		
		md.vertices.Add (new Vector3 (0, 0, 0));
		md.vertices.Add (new Vector3 (0, 1, 0));
		md.vertices.Add (new Vector3 (1, 1, 0));
		md.vertices.Add (new Vector3 (1, 0, 0));
		md.AddQuadTriangles ();
		md.vertices.Add (new Vector3 (0, 1, 0));
		md.vertices.Add (new Vector3 (0, 0, 0));
		md.vertices.Add (new Vector3 (1, 0, 0));
		md.vertices.Add (new Vector3 (1, 1, 0));
		md.AddQuadTriangles ();
		
		md.vertices.Add (new Vector3 (0, 0, 1));
		md.vertices.Add (new Vector3 (0, 1, 1));
		md.vertices.Add (new Vector3 (0, 1, 0));
		md.vertices.Add (new Vector3 (0, 0, 0));
		md.AddQuadTriangles ();
		md.vertices.Add (new Vector3 (0, 1, 1));
		md.vertices.Add (new Vector3 (0, 0, 1));
		md.vertices.Add (new Vector3 (0, 0, 0));
		md.vertices.Add (new Vector3 (0, 1, 0));
		md.AddQuadTriangles ();
	}

	private void Outline(Vector3 opos0, Vector3 opos1) {
		Vector3 pos0 = new Vector3 (Mathf.Min (opos0.x, opos1.x),Mathf.Min (opos0.y, opos1.y),Mathf.Min (opos0.z, opos1.z));
		Vector3 pos1 = new Vector3 (Mathf.Max (opos0.x, opos1.x),Mathf.Max (opos0.y, opos1.y),Mathf.Max (opos0.z, opos1.z));
//		pos0 -= Vector3.one / 1.99f;
//		pos1 += Vector3.one / 1.99f;

		pos0.y -= .501f;
		pos1.y += .501f;
		pos0.x -= .501f;
		pos1.x += .501f;
		pos0.z -= .501f;
		pos1.z += .501f;
		sBox.transform.position = pos0;
		sBox.transform.localScale = pos1 - pos0;
//		Debug.Log (sBox.transform.localScale);
		mrender.enabled = true;
	}

	public bool SetLayer(int nlayer) {
		y = nlayer;
		colliderPlane.transform.position = new Vector3 (colliderPlane.transform.position.x, y, colliderPlane.transform.position.z);
		sBox.transform.position = new Vector3 (sBox.transform.position.x, y, sBox.transform.position.z);
		return true;
	}
}
