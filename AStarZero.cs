﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Priority_Queue;

public static class AStarZero
{
	public class AStarNode : PriorityQueueNode
	{
		public PathNode Node { get; private set; }

		public AStarNode (PathNode n)
		{
			Node = n;
		}
	}

	private static bool raytrace (PathNode start, PathNode end)
	{
//		Debug.LogFormat ("Raytracing between {0},{1},{2} and {3},{4}", start.X, start.Y, start.Z, end.X, end.Z);
		int dx = Mathf.Abs (end.X - start.X);
		int dy = Mathf.Abs (end.Z - start.Z);
		int x = start.X;
		int y = start.Z;
		int n = 1 + dx + dy;
		int x_inc = (end.X > start.X) ? 1 : -1;
		int y_inc = (end.Z > start.Z) ? 1 : -1;
		int error = dx - dy;
		dx *= 2;
		dy *= 2;
		
		for (; n > 0; --n) {
			if (World.getBlock (x, start.Y, y).IsBlocking ()) {
				return false;
			}
			if (error > 0) {
				x += x_inc;
				error -= dy;
			} else {
				if ((error == 0) && (n > 1) && (World.getBlock (x + x_inc, start.Y, y).IsBlocking ())) {
					return false;
				} else
					y += y_inc;
				error += dx;
			}
		}
		return true;
	}

	public static Path search (PathNode start, PathNode end, bool _doRayTrace = true)
	{
//		Vector3 offset = new Vector3 (.5f, 0, .5f);
		List<PathNode> pathBlocks = new List<PathNode> ();
		PathNode current = null;
		HeapPriorityQueue<AStarNode> open = new HeapPriorityQueue<AStarNode> (World.chunkSize * World.chunkSize);
		Dictionary<PathNode,double> costSoFar = new Dictionary<PathNode, double> ();
		Dictionary<PathNode,PathNode> cameFrom = new Dictionary<PathNode,PathNode> ();
//		Dictionary<PathNode,List<PathEdge>> cacheCopy = Path.EdgeCache;
		HashSet<PathNode> closed = new HashSet<PathNode> ();
		double newCost, priority;
		open.Enqueue (new AStarNode (start), 0);
		costSoFar [start] = 0;
		cameFrom [start] = start;
		while (open.Count != 0) {
			current = open.Dequeue ().Node;
			Debug.DrawLine (current.Position, cameFrom [current].Position);
			if (current == end)
				break;
//			foreach (Block next in World.getBlock(current.Position).NeighborsOrtho(end)) {
//				if (!closed.Contains (next)) {
//					int newCost = costSoFar [current] + 1;
//					costSoFar [next] = newCost;
//					int priority = (int)(next.DistanceToBlock (end) * 2);
//					open.Enqueue (new AStarNode (next), priority);
//					cameFrom [next] = current;
//					closed.Add (next);
//				}
//			}
			List<PathEdge> neighbors = Path.EdgeCache [current];
			if (neighbors == null)
				continue;
			foreach (PathEdge next in neighbors) {
				if (next != null && !closed.Contains (next.End)) {
					newCost = costSoFar [current] + next.Cost ();
					costSoFar [next.End] = newCost;
					priority = next.End.H (end);
					open.Enqueue (new AStarNode (next.End), priority);
					cameFrom [next.End] = current;
					closed.Add (next.End);
				}
			}

		}
		if (current == end) {
			while (current != start) {
				pathBlocks.Add (current);
				current = cameFrom [current];
			}
			pathBlocks.Add (current);
		} else {
//			Debug.Log ("Path Not Found");
			return null;
		}
//		Debug.Log (path.Count);
//		Vector3 direction;
		if (_doRayTrace) {
			for (int i = 0; i < pathBlocks.Count - 2; i++) {
//				while ((i<path.Count-2) && (!Physics.Raycast(path[i].Position,direction = (path[i+2].Position-path[i].Position),direction.magnitude)))
				while ((i < pathBlocks.Count - 2) && raytrace (pathBlocks [i], pathBlocks [i + 2]))
					pathBlocks.RemoveAt (i + 1);
			}
		}
		if (pathBlocks.Count == 0)
			return null;
		pathBlocks.Reverse ();
		if (pathBlocks.Count > 1)
			return new Path (pathBlocks);
		else
			return null;
	}
}