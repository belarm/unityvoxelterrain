﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using C5;
using SCG =	System.Collections.Generic;
using System;
using Priority_Queue;

public static class PathFind
{

	private class AStarNode : PriorityQueueNode
	{
		public PathNode Node { get; private set; }

		public AStarNode (PathNode n)
		{
			Node = n;
		}
	}

	public static Path Search (PathNode start, PathNode end, QTNode graph)
	{
//		IntervalHeap<PathNode> openSet = new IntervalHeap<PathNode> ();
//		HeapPriorityQueue<AStarNode> open = new HeapPriorityQueue<AStarNode> (100);
		SortedList<int,Queue<PathNode>> openSet = new SortedList<int, Queue<PathNode>> ();
		Dictionary<PathNode,int> costSoFar = new Dictionary<PathNode, int> ();
		Dictionary<PathNode,PathNode> cameFrom = new Dictionary<PathNode,PathNode> ();
		C5.HashSet<PathNode> closed = new C5.HashSet<PathNode> ();
		List<PathNode> finalPath = new List<PathNode> ();
		PathNode current = null;
		int newCost, priority;
		openSet [0].Enqueue (start);
		costSoFar [start] = 0;
		cameFrom [start] = start;
		while (openSet.Count != 0) {
			//current = open.Dequeue ().Node;
			if (openSet.Values [0].Count > 0) {
				current = openSet.Values [0].Dequeue ();
			} else {
				//			if(pqs.Values[0].Count == 0)
				openSet.Remove (openSet.Keys [0]);
				continue;
			}
			if (current == end)
				break;
			closed.Add (current);
//			foreach (PathEdge nextedge in PathDB.FindEdges(current)) {
			foreach (PathEdge nextedge in graph.IntraEdges(current)) {
				if (nextedge != null && !closed.Contains (nextedge.End)) {
					newCost = costSoFar [current] + (int)nextedge.Cost ();
					costSoFar [nextedge.End] = newCost;
					priority = newCost + (int)Math.Ceiling (nextedge.End.H (end));
					if (!openSet.ContainsKey (priority)) {
						openSet.Add (priority, new Queue<PathNode> ());
					}
					openSet [priority].Enqueue (nextedge.End);


//					var anode = new AStarNode (nextedge.End);
//					if (!open.Contains (anode))
//						open.Enqueue (new AStarNode (nextedge.End), priority);
//					else
//						open.UpdatePriority (anode, priority);
					cameFrom [nextedge.End] = current;
				}
			}
		}
		if (current == end) {
			while (current != start) {
				finalPath.Add (current);
				current = cameFrom [current];
			}
			finalPath.Reverse ();
		} else {
			return null;
		}
		return finalPath.Count == 0 ? null : new Path (finalPath, graph.Level);
	}
}
