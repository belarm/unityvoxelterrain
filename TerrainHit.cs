﻿using UnityEngine;
using System.Collections;

public static class TerrainHit {

	public static Vector3 Constrain(Vector3 pos) {
		pos.x = Mathf.Min (Mathf.Max (pos.x, 0), World.sizex-1);
		pos.y = Mathf.Min (Mathf.Max (pos.y, 0), World.sizey-1);
		pos.z = Mathf.Min (Mathf.Max (pos.z, 0), World.sizez-1);
		return pos;
	}

	public static int[] GetBlockPos(Vector3 pos){
		int[] res = new int[] {Mathf.RoundToInt(pos.x),Mathf.RoundToInt(pos.y),Mathf.RoundToInt(pos.z)};
		return res;
	}

	public static Vector3 GetBlockPosV(Vector3 pos){
		Vector3 res = new Vector3 (Mathf.RoundToInt(pos.x),Mathf.RoundToInt(pos.y),Mathf.RoundToInt(pos.z));
		return res;
	}

	public static int[] GetBlockPos(RaycastHit hit, bool adjacent){
		Vector3 pos = new Vector3 (
			MoveWithinBlock (hit.point.x, hit.normal.x, adjacent),
			MoveWithinBlock (hit.point.y, hit.normal.y, adjacent),
			MoveWithinBlock (hit.point.z, hit.normal.z, adjacent)
		);
		return GetBlockPos (pos);
	}
	static float MoveWithinBlock(float pos, float norm, bool adjacent = false)
	{
		if (pos - (int)pos == 0.5f || pos - (int)pos == -0.5f)
		{
			if (adjacent)
			{
				pos += (norm / 2);
			}
			else
			{
				pos -= (norm / 2);
			}
		}
		
		return (float)pos;
	}

	 
	public static Block GetBlock(RaycastHit hit, bool adjacent = false)
	{
		int[] pos = GetBlockPos(hit, adjacent);
		
		Block block = World.getBlock(pos[0], pos[1], pos[2]);
		return block;
	}

}
