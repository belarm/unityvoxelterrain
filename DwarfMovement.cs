﻿using UnityEngine;
using System.Collections;

public class DwarfMovement : MonoBehaviour {
	public Vector3 destination;
	public float speed = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, destination, step);
	}
}
