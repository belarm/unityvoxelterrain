using UnityEngine;
using System.Collections;


public class CameraControl : MonoBehaviour
{

	public float horizontalSpeed = 40;
	public float verticalSpeed = 40;
	public float cameraRotateSpeed = 80;
	public float cameraDistance = 30;
	public float climbSpeed = 40;
	public int renderDepth = 5;
	public int layer = 0;
	public World gWorld;
	//[HideInInspector]
	public Camera mCam;
	private GameObject sphereMarker;
	Block endPoint = null;
	Block startPoint = null;
	//private Block selected;
	public int pathdepth = 0;

	void Start ()
	{
		sphereMarker = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphereMarker.gameObject.SetActive (false);
	}

	void Update ()
	{
		float horizontal = Input.GetAxis ("Horizontal") * horizontalSpeed * Time.deltaTime;
		float vertical = Input.GetAxis ("Vertical") * verticalSpeed * Time.deltaTime;
		float climb = Input.GetAxis ("Climb") * climbSpeed * Time.deltaTime;
		float rotation = Input.GetAxis ("Rotation");
		float vrot = Input.GetAxis ("Vrotation");
		transform.Translate (Vector3.forward * vertical);
		transform.Translate (Vector3.right * horizontal);
		transform.Translate (Vector3.up * climb);


		if (Input.GetKeyDown (KeyCode.KeypadMinus) && pathdepth > 0) {
			pathdepth--;
			Debug.LogFormat ("Set pathdepth to {0}", pathdepth);
		}
		if (Input.GetKeyDown (KeyCode.KeypadPlus) && pathdepth < World.maxDepth) {
			pathdepth++;
			Debug.LogFormat ("Set pathdepth to {0}", pathdepth);
		}

		if (Input.GetButtonDown ("DownLevel") && layer > 0) {
			for (int i = 0; i < World.sizex / World.chunkSize; i++) {
				for (int j = 0; j < World.sizez / World.chunkSize; j++) {
					if (layer > renderDepth) {
						World.chunks [i, layer - renderDepth - 1, j].enable ();
					}
					World.chunks [i, layer, j].disable ();
				}
			}
			gWorld.SetLayer (--layer);
		}
		if (Input.GetButtonDown ("UpLevel") && layer < World.sizey - 1) {
			for (int i = 0; i < World.sizex / World.chunkSize; i++) {
				for (int j = 0; j < World.sizez / World.chunkSize; j++) {
					if (layer >= renderDepth) {
						World.chunks [i, layer - renderDepth, j].disable ();
					}
					World.chunks [i, layer + 1, j].enable ();
				}
			}
			gWorld.SetLayer (++layer);
		}



		if (Input.GetKeyDown (KeyCode.F)) {
			RaycastHit hit;
			//Block hitBlock;
			Ray clickRay = mCam.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (clickRay, out hit, Mathf.Infinity, 1)) {
				startPoint = TerrainHit.GetBlock (hit, true);
				if (!startPoint.IsBlocking ()) {
					QTNode qt = gWorld.FindQuad (startPoint.Position, 4);
					qt.DrawQuad ();
					gWorld.FindPath2 (startPoint, endPoint);
				}
			}
		}

		if (Input.GetKeyDown (KeyCode.B)) {
			RaycastHit hit;
			Ray clickRay = mCam.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (clickRay, out hit, Mathf.Infinity, 1)) {
				startPoint = TerrainHit.GetBlock (hit, true);
//				if (!startPoint.IsBlocking ()) {
////					var qt = startPoint.GetQuad ();
//					var pn = PathDB.GetNode (startPoint.Position);
//
//					Debug.Log ("Node level: " + pn.Level);
//					foreach (var path in PathDB.FindEdges(pn)) {
//						if (path.IsInterLeaf == true)
//							path.Draw ();
//					}
//				}
			}
		}

		if (Input.GetKeyDown (KeyCode.N)) {
			RaycastHit hit;
			Ray clickRay = mCam.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (clickRay, out hit, Mathf.Infinity, 1)) {
				endPoint = TerrainHit.GetBlock (hit, true);
				if (!endPoint.IsBlocking ()) {
//					endPoint.SetStatus (1);
					QTNode qt;
					qt = endPoint.GetQuad ();
					qt.DrawQuad ();
					Debug.LogFormat ("Quad has {0} portal nodes and paths", qt.Portals.Count);
					//qt.Parent.CachePaths ();
					qt.Parent.DrawIfEmpty ();
					//Debug.LogFormat ("Quad dimensions: {0} {1} - {2} {3}", qt.X, qt.Y, qt.X + qt.Size (), qt.Y + qt.Size ());
					//foreach (var node in qt.Portals) {
					//	Debug.Log (node);
					//}
//					foreach (var epath in qt.PortalEdges) {
//						Debug.Log (epath);
//					}
//					QTNode qt = gWorld.FindQuad (endPoint.GetAddr (), endPoint.Y);
//					Debug.LogFormat ("Got node {0},{1} depth {2}", qt.X, qt.Y, qt.QuadLayer);
//					Debug.LogFormat ("Neighbors: {0}",qt.Portals ().Count);
//					Debug.Log ("Neighbors North: " + qt.FindNeighbors(QOrd.North).Count);
//					Debug.Log ("Neighbors South: " + qt.FindNeighbors(QOrd.South).Count);
//					Debug.Log ("Neighbors East: " + qt.FindNeighbors(QOrd.East).Count);
//					Debug.Log ("Neighbors West: " + qt.FindNeighbors(QOrd.West).Count);
				}
			}
		}

		if (Input.GetButtonDown ("Fire2")) {
			RaycastHit hit;
			//Block hitBlock;
			Ray clickRay = mCam.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (clickRay, out hit, Mathf.Infinity, 1)) {
				endPoint = TerrainHit.GetBlock (hit, true);
//				sphereMarker.transform.position = endPoint.Position;
//				sphereMarker.SetActive (true);
			}
/*			else {
				Debug.Log ("Missed!");
			}*/
		}

//		if (Input.GetButtonDown ("Apply")) {
//			gWorld.deleteBlock(selected);
//			//World.setBlock (selected.getPos (), new BlockAir ());
//		}

		if (Input.GetButtonDown ("DrawQuads")) {
			gWorld.DrawQuads (layer, pathdepth);
		}

		if (rotation != 0) {
			transform.Rotate (Vector3.up, rotation * cameraRotateSpeed * Time.deltaTime, Space.World);
		}
		if (vrot != 0) {
			transform.Rotate (transform.right, vrot * cameraRotateSpeed * Time.deltaTime, Space.World);
		}
	}
}
