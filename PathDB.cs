﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

//using C5;
//using SCG =	System.Collections.Generic;
using System;

//TODO: REWRITE THIS WHOLE FUCKING MESS WITHOUT C5

public sealed class PathDB
{
	public static Dictionary<NodePos,List<PathEdge>>[] Edges;
	public static Dictionary<NodePos,PathNode>[] NodeDB;

	static PathDB ()
	{
		Edges = new Dictionary<NodePos, List<PathEdge>>[World.sizey];
		NodeDB = new Dictionary<NodePos, PathNode>[World.sizey];
		for (int i = 0; i < World.sizey; i++) {
			NodeDB [i] = new Dictionary<NodePos, PathNode> ();
			Edges [i] = new Dictionary<NodePos, List<PathEdge>> ();
		}
	}

	public static PathNode AddNode (int _x, int _y, QTNode _parent)
	{
		int layer = _parent.Tree.Layer;
		PathNode n;
		NodePos np = new NodePos(_x,layer,_y);
		if (NodeDB [layer].ContainsKey (np))
			n = NodeDB [layer] [np];
		else
			n = new PathNode (_x, _y, _parent);
		if (!Edges [layer].ContainsKey (n.GetPos())) {
			Edges [layer].Add (n.GetPos(), new List<PathEdge> ());
		}
		return n;
	}

	public static void FindOrAddNode (ref PathNode _node)
	{
		if (NodeDB [_node.Y].ContainsKey (_node.GetPos ()))
			_node = NodeDB [_node.Y] [_node.GetPos ()];
		else {
			NodeDB [_node.Y].Add (_node.GetPos (), _node);
			if (!Edges [_node.Y].ContainsKey (_node.GetPos())) {
				Edges [_node.Y].Add (_node.GetPos(), new List<PathEdge> ());
			}
		}
	}

	public static PathEdge FindEdge (PathNode a, PathNode b)
	{
		List<PathEdge> edges = null;
		Edges [a.Y].TryGetValue (a.GetPos(), out edges);
		if (edges == null)
			return null;
		foreach (var edge in edges)
			if (edge.End == b)
				return edge;
		return null;
	}

	/// <summary>
	/// Adds the specified edge AND ITS REVERSE to PathDB.Edges
	/// </summary>
	public static void AddEdge (PathNode a, PathNode b)
	{
		var start = a;
		var end = b;
		FindOrAddNode (ref start);
		FindOrAddNode (ref end);
		var edge = new PathEdge (start, end);
		var reverse = edge.GetReverse ();

		if (!Edges [start.Y] [start.GetPos()].Contains (edge))
			Edges [start.Y] [start.GetPos()].Add (edge);
		if (!Edges [end.Y] [end.GetPos()].Contains (reverse))
			Edges [end.Y] [end.GetPos()].Add (reverse);
		//		if (edge.Level > start.Level) {
		//			start.Level = edge.Level;
		//			Nodes [start.Y].Update (start);
		//		}
		//		if (reverse.Level > end.Level) {
		//			end.Level = reverse.Level;
		//			Nodes [end.Y].Update (end);
		//		}
	}

	/// <summary>
	/// Adds the specified edge AND ITS REVERSE to PathDB.Edges
	/// </summary>
	/// <returns>The edge.</returns>
	public static void AddEdge (PathEdge p)
	{
		AddEdge (p.Start, p.End);
	}

//	public bool UpdateNode(PathNode _oldkey, PathNode _newkey) {
//		PathNode oldkey = Edges [_oldkey.Y].TryGetValue (_oldkey);
//	}

	#region Singleton pattern

	private static readonly PathDB instance = new PathDB ();

	private PathDB ()
	{
	}

	public static PathDB Instance {
		get {
			return instance;
		}
	}

	#endregion

	#region Debug methods

	public static void PrintEdges (int _layer)
	{
		//for (int i = 0; i <= World.maxDepth; i++)
		foreach (var edges in Edges[_layer]) {
//			Debug.LogFormat ("{0}[{1}] => {2}", edges.Key, edges.Key.Level, edges.Value.Count);
			foreach (var edge in edges.Value)
				edge.Draw ();
		}
	}

	#endregion

}




//======================================   Here there be dragons   ==============================================









//public sealed class PathDB_old
//{
//	public static C5.HashSet<PathNode>[] Nodes;
//	public static C5.HashDictionary<PathNode,List<PathEdge>>[,] Edges;
//	//	public static Dictionary<PathNode,List<PathEdge>> Edges;
//
//	static PathDB_old ()
//	{
//		Nodes = new C5.HashSet<PathNode>[World.sizey];
//		Edges = new HashDictionary<PathNode, List<PathEdge>>[World.sizey, World.maxDepth + 1];
//		for (int i = 0; i < World.sizey; i++) {
//			Nodes [i] = new C5.HashSet<PathNode> ();
//			for (int j = 0; j <= World.maxDepth; j++) {
//				Edges [i, j] = new HashDictionary<PathNode, List<PathEdge>> ();
//			}
//			Nodes [i].ItemsAdded += nodesAdded;
//			Nodes [i].ItemsRemoved += nodesRemoved;
//		}
//	}
//
//	public static PathNode AddNode (int _x, int _y, int _worldLayer)
//	{
//		var n = new PathNode (_x, _worldLayer, _y);
//		Nodes [_worldLayer].FindOrAdd (ref n);
//		return n;
//	}
//
//	//	public static PathNode AddNode (PathNode _node)
//	//	{
//	//		var tempnode = _node;
//	//		Nodes [_node.Y].FindOrAdd (ref tempnode);
//	//		return tempnode;
//	//	}
//
//	public static void FindOrAddNode (ref PathNode _node)
//	{
//		Nodes [_node.Y].FindOrAdd (ref _node);
//	}
//
//	//	public static PathNode GetNode (int _x, int _y, int _worldLayer)
//	//	{
//	//		var n = new PathNode (_x, _worldLayer, _y);
//	//		return Nodes [_worldLayer].Find (ref n) ? n : null;
//	//	}
//
//	//	public static PathNode GetNode (Vector3 pos)
//	//	{
//	//		var n = new PathNode (pos);
//	//		return Nodes [n.Y].Find (ref n) ? n : null;
//	//	}
//
//	/// <summary>
//	/// Adds the specified edge AND ITS REVERSE to PathDB.Edges
//	/// </summary>
//	public static void AddEdge (PathNode a, PathNode b, int _level)
//	{
//		var start = a;
//		var end = b;
//		Nodes [a.Y].FindOrAdd (ref start);
//		Nodes [a.Y].FindOrAdd (ref end);
//		var edge = new PathEdge (start, end, _level);
//		var reverse = edge.GetReverse ();
//		if (!Edges [start.Y, _level] [start].Contains (edge))
//			Edges [start.Y, _level] [start].Add (edge);
//		if (!Edges [end.Y, _level] [end].Contains (reverse))
//			Edges [end.Y, _level] [end].Add (reverse);
////		if (edge.Level > start.Level) {
////			start.Level = edge.Level;
////			Nodes [start.Y].Update (start);
////		}
////		if (reverse.Level > end.Level) {
////			end.Level = reverse.Level;
////			Nodes [end.Y].Update (end);
////		}
//	}
//
//	/// <summary>
//	/// Adds the specified edge AND ITS REVERSE to PathDB.Edges
//	/// </summary>
//	/// <returns>The edge.</returns>
//	public static void AddEdge (PathEdge p, int _level)
//	{
//		AddEdge (p.Start, p.End, _level);
//	}
//
//	//	public static void AddEdge (int x1, int y1, int x2, int y2, int _layer)
//	//	{
//	//		AddEdge (new PathNode (x1, _layer, y1), new PathNode (x2, _layer, y2));
//	//	}
//	//
//	public static List<PathEdge> FindEdges (PathNode n, int _level)
//	{
//		List<PathEdge> edges;
//		return Edges [n.Y, _level].Find (n, out edges) ? edges : null;
//	}
//
//	public static PathEdge FindEdge (PathNode a, PathNode b)
//	{
//		List<PathEdge> edges = null;
//		Edges [a.Y, Morton.FindLCR (a.Address, b.Address)].Find (a, out edges);
//		if (edges == null)
//			return null;
//		foreach (var edge in edges)
//			if (edge.End == b)
//				return edge;
//		return null;
//	}
//
//	//	public static int MaxPathCost (PathNode node)
//	//	{
//	//		var pn = node;
//	//		List<PathEdge> edges;
//	//		int i = -1;
//	//		Nodes [pn.Y].Find (ref pn);
//	//		Edges [pn.Y].Find (pn, out edges);
//	//		foreach (PathEdge edge in edges) {
//	//			i = Math.Max (i, (int)edge.Cost ());
//	//		}
//	//		return i;
//	//	}
//
//	public static int NodeCount {
//		get {
//			int count = 0;
//			foreach (C5.HashSet<PathNode> nodelist in Nodes)
//				count += nodelist.Count;
//			return count;
//		}
//	}
//
//	public static int PathCount {
//		get {
//			int pathcount = 0, i = 0, j = 0;
//			for (i = 0; i < World.sizey; i++)
//				for (j = 0; j < World.maxDepth; j++)
//					foreach (var edgelist in Edges[i,j])
//						pathcount += edgelist.Value.Count;
//			return pathcount;
//		}
//	}
//
//	#region Event callbacks
//
//	internal static void nodesAdded (object coll, ItemCountEventArgs<PathNode> eArgs)
//	{
//		for (int i = 0; i <= World.maxDepth; i++)
//			Edges [eArgs.Item.Y, i] [eArgs.Item] = new List<PathEdge> ();
//		if (!eArgs.Item.Parent.Portals.Contains (eArgs.Item))
//			eArgs.Item.Parent.Portals.Add (eArgs.Item);
//	}
//
//	internal static void nodesRemoved (object coll, ItemCountEventArgs<PathNode> eArgs)
//	{
//		for (int i = 0; i <= World.maxDepth; i++)
//			Edges [eArgs.Item.Y, i] [eArgs.Item] = null;
//		eArgs.Item.Parent.Portals.Remove (eArgs.Item);
//	}
//
//	#endregion
//
//	#region Singleton pattern
//
//	private static readonly PathDB instance = new PathDB ();
//
//	private PathDB_old ()
//	{
//	}
//
//	public static PathDB Instance {
//		get {
//			return instance;
//		}
//	}
//
//	#endregion
//
//	#region Debug methods
//
//	public static void PrintEdges (int _layer)
//	{
//		for (int i = 0; i <= World.maxDepth; i++)
//			foreach (var edges in Edges[_layer,i]) {
//				Debug.LogFormat ("{0}[{1}] => {2}", edges.Key, edges.Key.Level, edges.Value.Count);
//				foreach (var edge in edges.Value)
//					edge.Draw ();
//			}
//	}
//
//	#endregion
//}
