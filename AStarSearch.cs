﻿using System;
using System.Collections.Generic;
using UnityEngine;




//public class PriorityQueue<T>
//{
//	// I'm using an unsorted array for this example, but ideally this
//	// would be a binary heap. Find a binary heap class:
//	// * https://bitbucket.org/BlueRaja/high-speed-priority-queue-for-c/wiki/Home
//	// * http://visualstudiomagazine.com/articles/2012/11/01/priority-queues-with-c.aspx
//	// * http://xfleury.github.io/graphsearch.html
//	// * http://stackoverflow.com/questions/102398/priority-queue-in-net
//
//	private List<Tuple<T, int>> elements = new List<Tuple<T, int>>();
//
//	public int Count
//	{
//		get { return elements.Count; }
//	}
//
//	public void Enqueue(T item, int priority)
//	{
//		elements.Add(Tuple.Create(item, priority));
//	}
//
//	public T Dequeue()
//	{
//		int bestIndex = 0;
//
//		for (int i = 0; i < elements.Count; i++) {
//			if (elements[i].Item2 < elements[bestIndex].Item2) {
//				bestIndex = i;
//			}
//		}
//
//		T bestItem = elements[bestIndex].Item1;
//		elements.RemoveAt(bestIndex);
//		return bestItem;
//	}
//}


public static class AStarSearch
{
	public static void search (Quadtree graph, int sx, int sy, int ex, int ey)
	{
		Dictionary<QTNode, QTNode> cameFrom = new Dictionary<QTNode, QTNode> ();
		Dictionary<QTNode, int> costSoFar = new Dictionary<QTNode, int> ();
		QTNode current = null;
		float t = Time.realtimeSinceStartup;
		QTNode start = graph.Top.FindNodeXY (sx, sy, 3);
		QTNode end = graph.Top.FindNodeXY (ex, ey, 3);
		//Stack<QuadtreeNode> frontier = new Stack<QuadtreeNode> ();
		SortedList<int,Queue<QTNode>> openSet = new SortedList<int, Queue<QTNode>> ();
//		List<QuadtreeNode> neighbors = new List<QuadtreeNode> ();

		if (start == null)
			Debug.Log ("NULL START");
		if (end == null)
			Debug.Log ("NULL START");
		openSet.Add (0, new Queue<QTNode> ());
		openSet [0].Enqueue (start);
//		start.DrawPath (end,Color.cyan);
		
		cameFrom [start] = start;
		costSoFar [start] = 0;
		while (openSet.Count > 0) {
//			Debug.Log (pqs.Values[0].Count);
			if (openSet.Values [0].Count > 0) {
				current = openSet.Values [0].Dequeue ();
			} else {
//			if(pqs.Values[0].Count == 0)
				openSet.Remove (openSet.Keys [0]);
				continue;
			}
			if (current == end) {
				break;
			}
//			current.FindNeighbors(QOrd.North, neighbors);
//			current.FindNeighbors(QOrd.South, neighbors);
//			current.FindNeighbors(QOrd.East, neighbors);
//			current.FindNeighbors(QOrd.West, neighbors);
//			neighbors = current.Neighbors();
			foreach (QTNode next in current.Neighbors) {
//				next.DrawQuad();
				int newCost = costSoFar [current] + next.H (end);
				if (!costSoFar.ContainsKey (next) || newCost < costSoFar [next]) {
					costSoFar [next] = newCost;
					int priority = newCost + next.H (end) * (next.IsLeaf () ? 2 : 3);
					if (!openSet.ContainsKey (priority)) {
						openSet.Add (priority, new Queue<QTNode> ());
					}
					openSet [priority].Enqueue (next);
					//frontier.Push (next);
					cameFrom [next] = current;
				}
			}
//			neighbors.Clear();
		}
		Debug.Log ("Time to find path---: " + (t - Time.realtimeSinceStartup));
		while (current != start) {
//			current.DrawQuad ();
			current = cameFrom [current];
		}
	}

	public static void search2 (Quadtree graph, int sx, int sy, int ex, int ey)
	{
		Dictionary<QTNode, QTNode> cameFrom = new Dictionary<QTNode, QTNode> ();
		Dictionary<QTNode, int> costSoFar = new Dictionary<QTNode, int> ();
		QTNode current = null;
		float t = Time.realtimeSinceStartup;
		QTNode start = graph.Top.FindNodeXY (sx, sy, 3);
		QTNode end = graph.Top.FindNodeXY (ex, ey, 3);
		//Stack<QuadtreeNode> frontier = new Stack<QuadtreeNode> ();
		SortedList<int,Queue<QTNode>> openSet = new SortedList<int, Queue<QTNode>> ();
		List<QTNode> neighbors = new List<QTNode> ();
		
		if (start == null)
			Debug.Log ("NULL START");
		if (end == null)
			Debug.Log ("NULL START");
		openSet.Add (0, new Queue<QTNode> ());
		openSet [0].Enqueue (start);
		//		start.DrawPath (end,Color.cyan);
		
		cameFrom [start] = start;
		costSoFar [start] = 0;
//		uint cmorton;
//		QuadtreeNode[] ortho = new QuadtreeNode[4];
		while (openSet.Count > 0) {
			//			Debug.Log (pqs.Values[0].Count);
			if (openSet.Values [0].Count > 0) {
				current = openSet.Values [0].Dequeue ();
			} else {
				//			if(pqs.Values[0].Count == 0)
				openSet.Remove (openSet.Keys [0]);
				continue;
			}
			if (current == end) {
				break;
			}
			//			current.FindNeighbors(QOrd.North, neighbors);
			//			current.FindNeighbors(QOrd.South, neighbors);
			//			current.FindNeighbors(QOrd.East, neighbors);
			//			current.FindNeighbors(QOrd.West, neighbors);
			//			neighbors = current.Neighbors();
//			cmorton = current.GetMorton();
//			if(current.CanGo(QOrd.North))
//			neighbors.Add (graph.SafeFind(current.X - 1, current.Y,3));
//			neighbors.Add (graph.SafeFind(current.X, current.Y - 1,3));
//			neighbors.Add (graph.SafeFind(current.X + 1 << current.QuadLayer, current.Y,3));
//			neighbors.Add (graph.SafeFind(current.X, current.Y + QTNode.Size(current),3));
//
//			ortho[0] = current.X > 0 ? graph.Top.FindNode((uint)current.X-1,(uint)current.Y,3) : null;
//			ortho[1] = current.Y > 0 ? graph.Top.FindNode((uint)current.X,(uint)current.Y-1,3) : null;
//			ortho[2] = current.X < World.sizex - 1 ? graph.Top.FindNode((uint)current.X+1,(uint)current.Y,3) : null;
//			ortho[3] = current.Y < World.sizey - 1 ? graph.Top.FindNode((uint)current.X,(uint)current.Y-1,3) : null;
//			foreach (QuadtreeNode next in current.Neighbors(3)) {
			foreach (QTNode next in neighbors) {
//				QuadtreeNode next = ortho[i];
				if (next == null)
					continue;
				int newCost = costSoFar [current] + next.H (end);
				if (!costSoFar.ContainsKey (next) || newCost < costSoFar [next]) {
					costSoFar [next] = newCost;
//					int priority = newCost + next.H(end) * 1;
					int priority = newCost;
					if (!openSet.ContainsKey (priority)) {
						openSet.Add (priority, new Queue<QTNode> ());
					}
					openSet [priority].Enqueue (next);
					//frontier.Push (next);
					cameFrom [next] = current;
				}
			}
			neighbors.Clear ();
		}
		Debug.Log ("Time to find path2: " + (t - Time.realtimeSinceStartup));
		while (current != start) {
//			current.DrawQuad ();
			current = cameFrom [current];
		}
	}


}


